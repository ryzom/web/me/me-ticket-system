<?php
/**
* @package   me
* @subpackage support
* @author    me.ryzom.com
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/

class accountsCtrl extends Ry\UserController {
	
	public function banned() {
		$rep = $this->_getResponse();
		$rep->addCSSLink('/css/support.css');
		
		$db = Ry\DB::spawn('api~billing');
		$linked_accounts = $db->get('linked_accounts', ['ryzom_uid' => '827144']);
		$total_connected_linked_accounts = 0;
		if ($linked_accounts) {
			foreach($linked_accounts as $linked_account) {
				if (substr($linked_account->universal_id, 0, 8) == '#banned_' && strtotime($linked_account->banned) >= strtotime(date('Y-m-d'))) {
					$accounts = $db->get('linked_accounts', ['universal_id' => $linked_account->universal_id]);
					foreach($accounts as $account) {
						$status = Ry\DB::querySingle('ring_live', 'SELECT current_status FROM ring_users WHERE user_id='.$account->ryzom_uid);
						if ($status && $status->current_status == 'cs_online')
							$total_connected_linked_accounts++;
					}
				}
			}
		}
		$rep->body->assign('MAIN', '');
		p($total_connected_linked_accounts);
		return $rep;
	}
}

