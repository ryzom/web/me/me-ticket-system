<?php
/**
* @package   me
* @subpackage support
* @author    me.ryzom.com
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/

class searchCtrl extends Ry\UserController {
	
	public function __construct($request) {
		parent::__construct($request);
		$this->channels = new Support\Channels($this->ini, $this->user);
	}

	public function index() {
		$rep = $this->_getResponse('html');
		$rep->addCSSLink('/css/support.css');
		Ry\Common::includeJSandCSS($rep);
		Ry\Common::setupMessage($rep);
		if ($this->auth->isLogged()) {
			$id = $this->user->id;
			$tpl = new jTpl();
			
			$formUser = \jForms::create('search_account');
			$tpl->assign('search_user_form', $formUser);

			$formTicket = \jForms::create('search_ticket');
			if (isset($_SESSION['ME_SUPPORT_SEARCH_RESULTS']))
				$tpl->assign('search_saved_results', $_SESSION['ME_SUPPORT_SEARCH_RESULTS']);
			$tpl->assign('search_ticket_form', $formTicket);
			
			$tpl->assign('agent_id', $id);
			$rep->body->assign('show_user_id', 0);
			$rep->body->assign('show_user_name', $this->show_user->name);
			
			$channels = new Support\Channels($this->ini, $this->user);
			$channels = $channels->getForMenu();
			
			$rep->body->assign('channels', $channels);
			$rep->body->assign('MAIN', $tpl->fetch('search'));
		} else {
			$rep->body->assign('username', '#Guest');
		}
		
		return $rep;
	}
	
	public function user() {
		$rep = $this->getResponse('htmlfragment');
		if ($this->user->checkAccess('account:search')) {
			$search_category = array('UId', 'Login', 'FirstName', 'LastName', 'Email', 'Birthday', 'Country', 'Address', 'City', 'PostalCode', 'USState');

			$sort_array = $this->param('sort');
			$search_all = $this->param('all');
			
			$sort = [];
			$search = [];
			$search_fields = [];
			if ($search_all) {
				if ($sort_array['all'] == 'DESC')
					$sort = 'DESC';
				else
					$sort = 'ASC';
					
				$search_all =  str_replace('*', '%', trim($search_all));
				if (strlen($search_all) > 1) {
					foreach ($search_category as $category) {
						$search_fields[] = $search_all;
						$search[] = '`'.$category.'` LIKE %s';
					}
				}
				
				$search = implode(' OR ', $search).' ORDER BY UId '.$sort.' LIMIT 100';
			} else {
				$fields = [];
				foreach ($search_category as $category) {
					$field = $this->param($category);
					if ($this->param($category)) {
						$fields[$category] = trim($field);
						$search_fields[] = str_replace('*', '%', $fields[$category]);

						if ($sort_array[$category] == 'DESC')
							$sort[] = $category.' DESC';
						else
							$sort[] = $category.' ASC';
						

						$search[] = '`'.$category.'` LIKE %s';
					}
				}
				
				$search = implode(' AND ', $search).' ORDER BY '.implode(', ', $sort). ' LIMIT 100';
			}

			$total = Ry\Db::query('nel', 'SELECT COUNT(*) as total FROM user WHERE '.$search, $search_fields);
			$results = Ry\Db::query('nel', 'SELECT '.implode(', ', $search_category).' FROM user WHERE '.$search, $search_fields);

			$tpl = new \jTpl();
			$tpl->assign('categories', $search_category);
			$tpl->assign('total', $total[0]->total);
			$tpl->assign('results',$results);
			$result = $tpl->fetch('search_result');
			$_SESSION['ME_SUPPORT_SEARCH_RESULTS'] = $result;
			$rep->addContent($result);
		} else {
			$rep->addContent('no soup for you');
			$_SESSION['ME_SUPPORT_SEARCH_RESULTS'] = '';
		}
		return $rep;
	}
	
	public function ticket() {
		$rep = $this->getResponse('htmlfragment');
		if ($this->user->checkAccess('support:all')) {
			$search_category = array('id', 'user_id', 'contact', 'char', 'subject', 'message', 'assignee', 'channel', 'type', 'created_after', 'created_before', 'updated_after', 'updated_before');

			$sort_array = $this->param('sort');
			$search_all = $this->param('all');

			$sort = [];
			$search = [];
			$search_fields = [];
			if ($search_all) {
				if ($sort_array['all'] == 'ASC')
					$sort = 'ASC';
				else
					$sort = 'DESC';

				$search_all = trim($search_all);
				if ($search_all[0] != '"' && $search_all[0] != '\'')
					$search_all = '%'.$search_all;
				if (substr($search_all, -1) != '"' && substr($search_all, -1) != '\'')
					$search_all = $search_all.'%';
				$search_all =  str_replace('*', '%', trim($search_all));

				if (strlen($search_all) > 1) {
					foreach ($search_category as $category) {
						if (!in_array($category, ['created_after', 'created_before', 'updated_after', 'updated_before'])) {
							$search_fields[] = $search_all;
							$search[] = '`'.$category.'` LIKE %s';
						}
					}
				}
				
				$date_search = [];
				foreach ($search_category as $category) {
					$field = $this->param(ucfirst($category));
					if ($field) { 
						if (in_array($category, ['created_after', 'created_before', 'updated_after', 'updated_before'])) {
							$date = $field['year'].'-'.$field['month'].'-'.$field['day'];
							$date = $field['year'].'-'.$field['month'].'-'.$field['day'];
							if ($date != '--') {
								if ($category == 'created_after' || $catergory == 'updated_after')
									$search_fields[] = $date.' 00:00:00';
								else
									$search_fields[] = $date.' 23:59:59';
								if ($category == 'created_after')
									$search_date = '`creation_date` >= ';
								elseif ($category == 'created_before')
									$search_date = '`creation_date` <= ';
								elseif ($category == 'updated_after')
									$search_date = '`update_date` >= ';
								elseif ($category == 'updated_before')
									$search_date = '`update_date` <= ';
								$date_search[] = $search_date.' %s';
							}
						}
					}
				}
				
				if ($date_search)
					$date_search = ' AND '.implode(' AND ', $date_search);
				else
					$date_search = '';
				
				$search = '('.implode(' OR ', $search).')'.$date_search.' ORDER BY Id '.$sort.' LIMIT 100';
			} else {
				$sort[] = 'Id DESC';
				foreach ($search_category as $category) {
					$field = $this->param(ucfirst($category));
					if ($field) { 
						if ($category == 'type') {
							$search[] = '`'.$category.'` IN ("'.implode('", "', array_keys($field)).'")';
						} else if (in_array($category, ['created_after', 'created_before', 'updated_after', 'updated_before'])) {
							$date = $field['year'].'-'.$field['month'].'-'.$field['day'];
							if ($date != '--') {
								if ($category == 'created_after' || $catergory == 'updated_after')
									$search_fields[] = $date.' 00:00:00';
								else
									$search_fields[] = $date.' 23:59:59';
								if ($category == 'created_after')
									$search_date = '`creation_date` >= ';
								elseif ($category == 'created_before')
									$search_date = '`creation_date` <= ';
								elseif ($category == 'updated_after')
									$search_date = '`update_date` >= ';
								elseif ($category == 'updated_before')
									$search_date = '`update_date` <= ';
								$search[] = $search_date.' %s';
								
							}
						} else {
							$field = trim($field);
							if ($field[0] != '"' && $field[0] != '\'')
								$field = '%'.$field;
							if (substr($field, -1) != '"' && substr($field, -1) != '\'')
								$field = $field.'%';
							$field =  str_replace('*', '%', trim($field));
							
							$search_fields[] =  $field;
							$search[] = '`'.$category.'` LIKE %s';
						}
						
						if (!in_array($category, ['id', 'created_after', 'created_before', 'updated_after', 'updated_before'])) {
							if ($sort_array[$category] == 'DESC')
								$sort[] = '`'.$category.'` DESC';
							else
								$sort[] = '`'.$category.'` ASC';
						
						}
					}
				}
			 
				$search = implode(' AND ', $search).' ORDER BY '.implode(', ', $sort). ' LIMIT 100';
			}

			foreach ($search_category as $i => $category) 
				if (in_array($category, ['message', 'user_id', 'email', 'created_after', 'created_before', 'updated_after', 'updated_before']))
					unset($search_category[$i]);
			
			$results = Ry\Db::query('ryzocket', 'SELECT `'.implode('`, `', $search_category).'` FROM tickets WHERE '.$search, $search_fields);
			foreach($results as $result) {
				$result->subject = substr($result->subject, 0, 48);
				if ($this->channels->canAccessChannel($result->channel))
					$final[] = $result;
			}
			
			$tpl = new \jTpl();

			$tpl->assign('categories', $search_category);
			$tpl->assign('total', count($final));
			$tpl->assign('results', $final);
			$rep->addContent($tpl->fetch('search_result'));
		}
		return $rep;
	}
	
	
}

