<?php
/**
* @package   me
* @subpackage support
* @author    me.ryzom.com
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/

class defaultCtrl extends Ry\UserController {
	
	function index() {
		return $this->redirect(jUrl::getFull('support~search:index'));
		
		/// OLD
		
		$rep = $this->_getResponse('html');
		Ry\Common::includeJSandCSS($rep);
		
		$id = $this->user->id;
		$form = \jForms::create('search_account');
		$tpl = new jTpl();
		$tpl->assign('agent_id', $id);
		$tpl->assign('form', $form);
		$tpl->assign('user', $this->user->infos);
		$rep->body->assign('MAIN', $tpl->fetch('support'));
		
		return $rep;
	}
	
	public function search() {
		$rep = $this->getResponse('htmlfragment');
		$search_category = array('UId', 'Login', 'FirstName', 'LastName', 'Email', 'Birthday', 'Country', 'Address', 'City', 'PostalCode', 'USState');

		$sort_array = $this->param('sort');
		$search_all = $this->param('all');
		
		$sort = [];
		$search = [];
		$search_fields = [];
		if ($search_all) {
			if ($sort_array['all'] == 'DESC')
				$sort = 'DESC';
			else
				$sort = 'ASC';
				
			$search_all =  str_replace('*', '%', trim($search_all));
			if (strlen($search_all) > 1) {
				foreach ($search_category as $category) {
					$search_fields[] = $search_all;
					$search[] = '`'.$category.'` LIKE %s';
				}
			}
			
			if ($this->user->checkAccess('support:search')) {
				$formUser = \jForms::create('search_account');
				$tpl->assign('search_user_form', $formUser);
				$formTicket = \jForms::create('search_ticket');
				$tpl->assign('search_ticket_form', $formTicket);
				$tpl->assign('ticket_form', null);
				$tpl_name = 'search';
			} else {
				$form = jForms::create('ticket');
				$form->setData('char', $this->user->name);
				$tpl->assign('search_form', null);
				$tpl->assign('ticket_form', $form);
			}
				
			$tpl->assign('agent_id', $id);
			$rep->body->assign('show_user_id', 0);
			$rep->body->assign('show_user_name', $this->show_user->name);
			
			$channels = new Support\Channels($this->ini, $this->user);
			$channels = $channels->getForMenu();
			
			$rep->body->assign('channels', $channels);
		
			$rep->body->assign('MAIN', $tpl->fetch($tpl_name));
		} else {
			$rep->body->assign('username', '#Guest');
		}

		return $rep;
	}
	
	public function sendmail() {
			$rep = $this->_getResponse('html');
		Ry\Common::includeJSandCSS($rep);
		
		$id = $this->user->id;
		$create = false;
		$form = NULL;
		try {
			$form = jForms::fill('send_email');
		} catch(Exception $e) {
			$create = true;
		}
		
		if (!$form || $create)
			$form = jForms::create('send_email');

		if (!$create && $this->show_user_id && $form && $form->check()) {
			$subject = $this->param('subject');
			$message = $this->param('message');
			$this->utils->sendEmailWithTemplate($subject,
				'api~mail_content_generic',
				 $message,
				$values=[
					'bgcolor' => 'orange',
					'color' => 'black',
					'infos' => $subject,
					'text' => $message],
				$this->show_user_id);
			Ry\Common::log('support', 'email', 'info', $subject."\n\n".$message, $this->show_user_id);
			$rep->body->assign('MAIN', 'Email sent');
		} else {
			$tpl = new jTpl();
			$tpl->assign('agent_id', $id);
			$tpl->assign('show_user_id', $this->show_user_id);
			$tpl->assign('form', $form);
			$tpl->assign('user', $this->user->infos);
			$tpl->assign('player_name', $this->show_user->name);
			$rep->body->assign('MAIN', $tpl->fetch('send_email'));
		}
		
		return $rep;
	}
	

}

