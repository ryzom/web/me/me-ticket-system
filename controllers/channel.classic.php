<?php
/**
* @package   me
* @subpackage support
* @author    me.ryzom.com
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/

class channelCtrl extends Ry\Controller {
	
	public function index() {
		
		$tpl = new jTpl();

		$is_agent = $this->user->checkAccess('support:all');
		
		$channels = new Support\Channels($this->ini, $this->user);
		$channel = $this->param('name');
		$ig = $this->user->ig;
		$rep = $this->_getResponse();
		$rep->addCSSLink('/css/support.css');
		Ry\Common::setupMessage($rep);

		if ($this->auth->isLogged()) {
			list($valid_channels, $query_channels) = $channels->getUserChannels();
			if ($channel && (($channel != 'My Tickets' && $channel[0] != '-' && !$channels->canAccessChannel($channel)) || ($channel == 'My Tickets' && $query_channels == '')))
				$channel = '';
			$rep->body->assign('channels', $channels->getForMenu($this->auth->isLogged(), $this->params()));
			$rep->body->assign('selected_channel', $channel);
			$tpl->assign('ig', $ig);
		} else {
			$channel = '';
			$rep->body->assign('channels', $channels->getForMenu($this->auth->isLogged(), $this->params()));
			$rep->body->assign('selected_channel', '');
		}
		
		if (!$is_agent)
			$channel = 'My Tickets';
		
		$db = Ry\DB::spawn('support~ryzocket');
		
		$guest_email = $this->param('email');
		$guest_token = $this->param('token');
		
		if (!$this->auth->isLogged()) {
			if (!$guest_email || !$guest_token)
				return $rep;
		}
		
		$closed_tickets = [];

		if (!$channel || $channel == 'My Tickets' || $channel == '*NEW*' || $channel[0] == '-') {
			if ($this->auth->isLogged() && $channel == 'My Tickets') {
				// Agent My Tickets
				if ($is_agent) {
					$fields = ['id', 'channel', 'subject', 'creation_date'];
						$tickets = Ry\DB::query('ryzocket', 'SELECT * FROM tickets WHERE assignee = %s AND type IN ("new", "open", "done", "wip") ORDER BY FIELD(type, "new", "wip", "open", "done"), update_date DESC', [$this->user->char_name]);

//					$tickets = $db->get('tickets', ['assignee' => $this->user->char_name, '#ORDER BY' => [['FIND_IN_SET(id, "new, wip, open, done")'], ['update_date', 'DESC']]]);
					$closed_tickets = Ry\DB::query('ryzocket', 'SELECT * FROM tickets WHERE assignee = %s AND type = "closed" ORDER BY update_date DESC LIMIT 10', [$this->user->char_name]);
				} else {
				// Player My Tickets
					$fields = ['id', 'channel', 'subject', 'creation_date'];
					$tickets = Ry\DB::query('ryzocket', 'SELECT * FROM tickets WHERE user_id = %s ORDER BY FIELD(type, "new", "waiting", "wip", "open", "done", "close") DESC, update_date DESC', [$this->user->id]);
					//$tickets = $db->get('tickets', ['user_id' => $this->user->id, '#ORDER BY' => [['FIND_IN_SET(type, "new, wip, open, done")'], ['update_date', 'DESC']]]);
				}
			} else if ($channel == '*NEW*') {
				$fields = ['id', 'channel', 'contact', 'subject', 'creation_date'];
				$temp_tickets = $db->get('tickets', ['type' => 'new'], ['#ORDER BY' => [['creation_date', 'DESC']]]);
				$tickets = [];
				foreach($temp_tickets as $ticket) {
					if ($channels->canAccessChannel($ticket->channel))
						$tickets[] = $ticket;
				}
			} elseif ($channel && $channel[0] == '-') {
				// Agent Show Player Tickets
				$fields = ['id', 'channel', 'subject', 'creation_date'];
				if ($this->show_user_id)
					$tickets = Ry\DB::query('ryzocket', 'SELECT * FROM tickets WHERE user_id = %s ORDER BY FIELD(type, "new", "waiting", "wip", "open", "done", "close") DESC,  update_date DESC', [$this->show_user_id]);
			} else {
				// Players Tickets
				$fields = ['id', 'assignee', 'subject', 'creation_date'];
				if (!$this->auth->isLogged()) {
					$tickets = Ry\DB::query('ryzocket', 'SELECT * FROM tickets WHERE email = %s AND mail_id = %s ORDER BY FIELD(type, "new", "done", "open", "wip", "closed"), update_date DESC', [$guest_email,  $guest_token]);
				} else
					$tickets = $db->get('tickets', ['user_id' => $this->user->id, '#ORDER BY' => [['FIND_IN_SET(type, "new, wip, open, done, close")'], ['update_date', 'DESC']]]);
			}
		} else { 
			// Agent have access to a channel
			$tickets = Ry\DB::query('ryzocket', 'SELECT * FROM tickets WHERE channel = %s AND type IN ("new", "open", "done", "wip") ORDER BY FIELD(type, "new", "wip", "open", "done"), update_date DESC', [$channel]);
			$closed_tickets = Ry\DB::query('ryzocket', 'SELECT * FROM tickets WHERE channel = %s AND type = "closed" ORDER BY update_date DESC LIMIT 10', [$channel]);
			$fields = ['id', 'assignee', 'contact', 'subject', 'creation_date'];
		}
		
		if ($is_agent) {
			$icons = ['new' => '32/new', 'open' => '32/eye', 'wip' => '32/new_message.gif', 'closed' => '32/bullet_red', 'done' => '32/hourglass', 'spam' => '32/bullet_delete'];
			foreach($icons as $name => $value)
				$icons[$name] = Ry\Common::i($value, _t('ticket_agent_'.$name));
		} else {
			$icons = ['waiting' => '32/email_error','new' => '32/hourglass', 'open' => '32/comments', 'wip' => '32/hourglass', 'closed' => '32/tick', 'done' => '32/new_message.gif', 'spam' => '32/bullet_delete'];
			foreach($icons as $name => $value)
				$icons[$name] = Ry\Common::i($value, _t('ticket_'.$name));
		}
		
		$tpl->assign('icons', $icons);

		$wkr = new jWiki('markdown_to_xhtml');
		$fields[] = 'remaining_time';
		foreach($tickets as $ticket) {
			if ($channel != '')
				$type =  ['message', 'note'];
			else
				$type = 'message';

			$note = $db->getSingle('tickets_notes', ['parent_id' => $ticket->id, 'type' => $type, '#ORDER BY' => [['creation_date', 'DESC'], ['id']]]);
			
			if ($ticket->update_date == '0000-00-00 00:00:00')
				$ticket->update_date = $ticket->creation_date;
			
			if ($ticket->update_date) {
				$dt = new jDateTime();
				$dt->setFromString($ticket->update_date, jDateTime::DB_DTFORMAT);
				$dt2 = new jDateTime();
				$dt2->now();
				$duration = $dt->durationTo($dt2);
				$hours = floor($duration->seconds/(60*60));
				$minutes = sprintf('%\'.02d', floor(($duration->seconds - ($hours*60*60)) / 60));
				$ticket->remaining_time = ($duration->days>1?$duration->days.' '._t('me~days'):'').' '.$hours.'h'.$minutes;
				if ($duration->days>15)
					$color='#FF4343';
				elseif ($duration->days>7)
					$color='pink';
				elseif ($duration->days>3)
					$color='#F9CB76';
				elseif ($duration->days>1)
					$color='yellow';
				$ticket->remaining_time = '<span style="font-weight: bold; color: '.$color.'">'.$ticket->remaining_time.'</span>';
			}

			if ($note) {
				$message = explode("\n", $note->message);
				//$message = str_replace("\n", "<br />\n", $message[0]);//$wkr->render(str_replace("\n", "<br />\n", $message[0])."\n");
				$message = $wkr->render(str_replace("\n", "<br />\n", $message[0])."\n");
				$ticket->subject = (trim($ticket->subject)?'<span class="ticket-message">'.$ticket->subject.'</span><br />':'').
					'<span class="ticket-note-message">'.Ry\Common::i('16/'.($note->type == 'note'?'note':($note->sender == 'agent'?'user_suit':'status_online'))).' '.
					$message.'</span>';
			} else {
				$message = explode("\n", trim($wkr->render($ticket->message)));
				$message = str_replace("\n", "<br />\n", preg_replace("/\n+/", "\n", $message[0]))."\n";
				$ticket->subject = (trim($ticket->subject)?'<span class="ticket-message">'.$ticket->subject.'</span><br />':'').'<span class="ticket-note-message">'.$message.'</span>';
			}
		}
		if ($closed_tickets)
			$closed_tickets = array_merge(['---'], $closed_tickets);
		$tpl->assign('agent', $channel != '' && $channel != 'My Tickets');
		$tpl->assign('sgm', $is_agent && $this->user->inGroup('SGM'));
		$tpl->assign('icons_user', $icons);
		$tpl->assign('fields', $fields);
		$tpl->assign('tickets', array_merge($tickets, $closed_tickets));
		$tpl->assign('email', $this->param('email'));
		$tpl->assign('token', $this->param('token'));
		$tpl->assign('channels', $channels->channels);
		$tpl_content = $tpl->fetch('tickets');
		
		$rep->body->assign('email', $this->param('email'));
		$rep->body->assign('token', $this->param('token'));
		$rep->body->assign('MAIN', $tpl_content);
		return $rep;
	}
}

