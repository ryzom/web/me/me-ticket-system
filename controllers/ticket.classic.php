<?php
/**
* @package   me
* @subpackage support
* @author    me.ryzom.com
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/
 
class ticketCtrl extends Ry\Controller {

	public function __construct($request) {
		parent::__construct($request);
		$this->channels = new Support\Channels($this->ini, $this->user);
		$this->form_channels = $this->channels->getFormChannels();
	}
	
	public function index() {
		$rep = $this->_getResponse();
		//$rep->addJSLink('/js/jquery.js');
		$rep->addJSLink(jApp::urlJelixWWWPath().'jquery/jquery.min.js');
		$rep->addJSLink(jApp::urlJelixWWWPath().'markitup/jquery.markitup.js');
		$rep->addJSLink('/js/markitup_markdown_set.js');
		$rep->addCSSLink('/css/jtageditor/style.css');
		$rep->addCSSLink('/css/markitup_markdown_style.css');
		$rep->addJSLink('/js/lightbox.js');
		$rep->addCSSLink('/css/lightbox.css');
		$rep->addJSLink('/js/dropzone.js');
		$rep->addCSSLink('/css/dropzone.css');
		$rep->addCSSLink('/css/support.css');
		
		$db = Ry\DB::spawn('support~ryzocket');
		
		$ticket_id = $this->param('ticket_id');
		$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
		if (!$ticket)
			$this->redirect(jUrl::getFull('me~index'));

		if ($this->auth->isLogged() && $ticket->user_id && $ticket->user_id != $this->user->id && $ticket->user_id != $this->show_user_id)
			$this->redirect(jUrl::getFull('support~ticket:index', ['ticket_id' => $ticket_id, 'show_user' => $ticket->user_id]));

		$ticket->channel = ucfirst($ticket->channel);
		$rep->body->assign('selected_channel', $ticket->channel);
		$rep->body->assign('ticket_id', $ticket_id);
		
		if ($this->show_user_id)
			$wkr = new jWiki('markdown_to_xhtml');
		else
			$wkr = new jWiki('markdown_to_text');
		
		$note = $wkr->render(str_replace('&amp;', '&', htmlspecialchars($this->param('note'), ENT_NOQUOTES)));
		$message = $wkr->render(str_replace('&amp;', '&', htmlspecialchars($this->param('message'), ENT_NOQUOTES)));
		$show_user = $this->param('show_user');

		$guest_email = $this->param('email');
		$guest_token = $this->param('token');
		$rep->body->assign('email', $guest_email);
		$rep->body->assign('token', $guest_token);

		if (!$this->auth->isLogged() && (!$guest_email || !$guest_token))
			$this->redirect(jUrl::getFull('me~index'));
		
		$tpl = new jTpl();

		$channels = $this->channels->getForMenu($this->auth->isLogged(), $this->params());
		$rep->body->assign('channels', $channels);
		$tpl->assign('channels', $channels);
	
		if ($this->auth->isLogged())
			$tpl->assign('ig', $this->user->ig);
		
		$is_agent = $this->user->checkAccess('support:all');
		$is_sgm = $is_agent && $this->user->inGroup('SGM');
		$is_dev = $is_agent && $this->user->inGroup('DEV');
	
		if (
				($ticket->user_id != 0 && (
					(($is_agent && $ticket->user_id == $this->show_user_id)
					||
					$ticket->user_id == $this->user->id))
				)
			||
				($this->channels->canAccessChannel($ticket->channel) ||
				($guest_email && $ticket->email == $guest_email && $ticket->mail_id == $guest_token))
			) 
		{
			
			if ($this->auth->isLogged()) {
				$user_id =  $this->user->id;
				$user_name = $this->user->name;
			} else {
				$user_id = 0;
				$user_name = $guest_email;
				if ($ticket->type == 'waiting')
					$db->update('tickets', ['type' => 'new'], ['id' => $ticket_id]);
			}

			$user = \Ry\Account::spawn($this->show_user_id);
			$lang = $user->infos->Community;
			
			if ($note) {
				$db->insert('tickets_notes', ['user_id' => $user_id, 'type' => 'note', 'creation_date' => Ry\DB::today(), 'email' => '', 'subject' => '', 'message' => $note, 'attachments' => '', 'contact' => $user_name, 'parent_id' => $ticket_id, 'sender' => 'agent']);
			} else if ($message) {
				$db->insert('tickets_notes', ['user_id' => $user_id, 'type' => 'message', 'creation_date' => Ry\DB::today(), 'email' => '', 'subject' => '', 'message' => $message, 'attachments' => '', 'contact' => $user_name, 'parent_id' => $ticket_id, 'sender' => $this->show_user_id?'agent':'user']);
				$type = $ticket->type;
				if ($type != 'new' || $this->show_user_id)
					$type = $this->show_user_id?'done':'wip';
				$db->update('tickets', ['update_date' => Ry\DB::today(), 'type' => $type], ['id' => $ticket_id]);
				if ($this->show_user_id) {
					$urlig = 'https://apps.ryzom.com/support/ticket?ticket_id='.$ticket_id;
					$url = jUrl::getFull('support~ticket:index', ['ticket_id' => $ticket_id, 'email' => $ticket->email, 'token' => $ticket->mail_id]);
					$subject = _t('you_receive_message', [$ticket_id, $ticket->subject], True, $lang);
					$this->utils->sendEmailWithTemplate('Re: '.$ticket->subject,
						'api~mail_content_generic',
						 '<a style="color: orange" href="'.$url.'">'.$url.'</a>',
						$values=[
							'bgcolor' => 'orange',
							'color' => 'black',
							'infos' => $subject.'<br/><br/>'._t('this_is_an_automatic_email', NULL, False, $lang),
							'text' => '<a style="color: orange" href="'.$url.'">'._t('click_to_read', NULL, False, $lang).'</a>'],
						$this->show_user_id);
					$this->utils->sendIGEmail('#'.$this->show_user_id, 'Re: '.$ticket->subject, $subject.'<br/><a style="color: orange" href="'.$urlig.'">'.$urlig.'</a>', 'Ryzom Support');
				}
			}
			
			if ($note || $message) {
				if ($this->show_user_id)
					$this->redirect(jUrl::get('support~ticket:index', ['show_user' => $this->show_user_id, 'ticket_id' => $ticket_id, 'email' => $guest_email, 'token' => $guest_token]));
				else
					$this->redirect(jUrl::get('support~channel:index', ['ticket_id' => $ticket_id, 'email' => $guest_email, 'token' => $guest_token]));
			}
			
			if (($ticket->type == 'done' && !$this->show_user_id)) {
				$db->update('tickets', ['type' => 'open'], ['id' => $ticket_id]);
				$db->insert('tickets_notes', ['subject' => 'set_status', 'parent_id' => $ticket_id, 'user_id' => $user_id, 'contact' => $user_name, 'message' => 'read', 'sender' => 'user', 'type' => 'action', 'creation_date' => Ry\DB::today()]);
			}
			
			$user_lang = Ry\Common::getUserLang();

			$agents = [];
			if ($is_agent)
				$agents = $this->channels->getChannelAgents($ticket->channel);
			
			if ($this->show_user_id)
				$notes = $db->get('tickets_notes', ['parent_id' => $ticket_id, '#ORDER BY' => [['creation_date'], ['id']]]);
			else
				$notes = $db->get('tickets_notes', ['parent_id' => $ticket_id, 'type' => ['screen', 'message'], '#ORDER BY' => ['creation_date']]);

			$sticket = sprintf('%09d', $ticket_id);
			$folder = $sticket[strlen($sticket)-3].'/'.$sticket[strlen($sticket)-2].'/'.$sticket[strlen($sticket)-1];

			foreach($notes as $id => $note) {
				if ($note->sender == 'user' && $ticket->char && ($ticket->char != '?' || !$is_sgm))
					$note->contact = $ticket->char;
				
				if ($note->sender == 'agent') {
					$url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:&])~i'; 
					$note->message = preg_replace($url, '<a href="$0" target="_blank" title="$0" style="padding: 2px;background-color: #000000aa">$0</a>', $note->message);
					$url = '~#([0-9]+)(?![^ ])~'; 
					$note->message = preg_replace($url, '<a href="https://me.ryzom.com//support/ticket/?ticket_id=$1" target="_blank" title="$0" style="padding: 2px;background-color: #000000aa">Ticket $0</a>', $note->message);
				}

				if ($note->subject == 'add_screen')
					$note->message = '<a href="/uploads/screens/'.$folder.'/'.$note->message.'" data-lightbox="image-box" data-title="'.$note->message.'"><img style="width: 32px; max-height: 32px" src="/uploads/screens/'.$folder.'/'.$note->message.'" /></a>';
				$note->translation = $note->$user_lang;
			}


			$screens = json_decode($ticket->attachments, true);
			$uploads = [];
			if (is_array($screens))
				foreach($screens as $screen)
					$uploads[$screen] = '/uploads/screens/'.$folder.'/'.urlencode($screen);
			
			$dt = new jDateTime();
			$last_update = ($ticket->update_date=='0000-00-00 00:00:00'?$ticket->creation_date:$ticket->update_date);
			$dt->setFromString($last_update, jDateTime::DB_DTFORMAT);
			$dt2 = new jDateTime();
			$dt2->now();
			$duration = $dt->durationTo($dt2);
			$ticket->remaining_time = $duration->days;
			$color = 'blue';
			switch($ticket->type) {
				
				case 'closed':
					$color = 'red';
					break;
				
				case 'spam':
					$color = 'violet';
					break;

				case 'open':
					$color = 'green';
					break;
				
				case 'wip':
					$color = 'orange';
					break;
				
				case 'waiting':
					$color = 'gray';
					break;
				
			}
			
			$tpl->assign('formUrl', jUrl::get('support~ticket:index', ['show_user' => $this->show_user_id, 'ticket_id' => $ticket_id, 'email' => $guest_email, 'token' => $guest_token]));
			$tpl->assign('changeChannelUrl', jUrl::get('support~ticket:changeChannel', ['show_user' => $this->show_user_id, 'ticket_id' => $ticket_id]));
			$tpl->assign('changeAgentUrl', jUrl::get('support~ticket:changeAgent', ['show_user' => $this->show_user_id, 'ticket_id' => $ticket_id]));
			$tpl->assign('changeLangUrl', jUrl::get('support~ticket:changeLang', ['show_user' => $this->show_user_id, 'ticket_id' => $ticket_id]));
			$tpl->assign('deeplizeUrl', jUrl::get('support~ticket:deeplize', ['show_user' => $this->show_user_id, 'ticket_id' => $ticket_id]));
			$message = $ticket->message;
			if ($this->show_user_id != '') {
				$smessage = explode('[SGM]', $message);
				if (count($smessage) == 3)
					$ticket->message = $smessage[1];
			} else {
				$smessage = explode('[PLAYER]', $message);
				if (count($smessage) == 3)
					$ticket->message = $smessage[1];
			}
			
			$char_id = 0;
			if ($is_agent && $ticket->char != '?') {
				$char_id = Ry\DB::querySingle('ring_live', 'SELECT char_id FROM characters WHERE char_name = %s', [$ticket->char]);
				if ($char_id)
					$char_id = $char_id->char_id;
			}
		
			if ($ticket->char != '?' || !$is_sgm)
				$contact = $ticket->char;
			else
				$contact = $ticket->contact;

			$linked_accounts = [];
			if ($is_sgm) {
				if (strpos($contact, '@') !== False) {
					$results = Ry\DB::query('nel', 'SELECT Login, UId FROM user WHERE Email = %s', $contact, true);
					foreach($results as $acc)
						$linked_accounts[] = [$acc->Login, $acc->UId];
				}
			}
			
			$ticket->translation = $ticket->$user_lang;
			
			$tpl->assign('ticket', $ticket);
			
			$tpl->assign('uploads', $uploads);
			$tpl->assign('notes', $notes);
			$tpl->assign('show_user', $show_user);
			$tpl->assign('ticket_color', $color);
			$tpl->assign('lang', $user_lang);
			$tpl->assign('agents', $agents);
			$tpl->assign('agent', $is_agent);
			$tpl->assign('sgm', $is_sgm);
			$tpl->assign('dev', $is_dev);
			$tpl->assign('contact', $contact);
			$tpl->assign('linked_accounts', $linked_accounts);
			$tpl->assign('user_name', $user_name);
			$tpl->assign('char_id', $char_id);
			$tpl->assign('email', $guest_email);
			$tpl->assign('token', $guest_token);
			$rep->body->assign('channels', $channels);
			$rep->body->assign('email', $guest_email);
			$rep->body->assign('token', $guest_token);
			$rep->body->assign('MAIN', $tpl->fetch('ticket'));
		} else {
			$tpl->assign('message', 'no soup for you!');
			$rep->body->assign('MAIN', $tpl->fetch('api~page_error'));
		}
		
		return $rep;
	}

	public function getMenu() {
		$rep = $this->getResponse('htmlfragment');
		$channels = $this->channels->getForMenu($this->auth->isLogged(), $this->params());
		$tpl = new jTpl();
		$tpl->assign('channels', $channels);
		$tpl->assign('selected_channel', $this->param('selected_channel'));
		$tpl->assign('show_user_id', is_numeric($this->show_user_id)?$this->show_user_id:0);
		$tpl->assign('email', $this->param('email'));
		$tpl->assign('token', $this->param('token'));
		$rep->addContent($tpl->fetch('menu'));
		return $rep;
		
	}

	public function create() {
		$rep = $this->_getResponse('html');
		$rep->addJSLink(jApp::urlJelixWWWPath().'jquery/jquery.min.js');
		$rep->addJSLink(jApp::urlJelixWWWPath().'markitup/jquery.markitup.js');
		$rep->addJSLink('/js/markitup_markdown_set.js');
		$rep->addCSSLink('/css/jtageditor/style.css');
		$rep->addCSSLink('/css/markitup_markdown_style.css');
		$rep->addJSLink('/js/lightbox.js');
		$rep->addCSSLink('/css/lightbox.css');
		$rep->addJSLink('/js/dropzone.js');
		$rep->addCSSLink('/css/dropzone.css');
		$rep->addCSSLink('/css/support.css');
	
		$channel = $this->param('channel', 'intro');
		$ticket_form = 'ticket_'.$channel;
		
		$wkr = new jWiki('markdown_to_xhtml');
		$help = str_replace("\n", "<br />\n", _t($ticket_form.'_help'));
		//$screen = str_replace("\n", "<br />\n", _t('add_screens'));
		$screen = '';
		$notes = str_replace("\n", "<br />\n", _t($ticket_form.'_notes', NULL, true));
		$infos = str_replace("\n", "<br />\n", _t($ticket_form.'_infos', NULL, true));

		if ((isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Ryzom') !== false))
			$help = $help."<br />\n".$screen;

		$help = $wkr->render($help);
		$help = str_replace('href=', 'target="_blank" href=', $help);
		p($help);
		$notes = $wkr->render($notes);
		$tpl = new jTpl();
		$form = jForms::create($ticket_form);
		$char_select = '';
		$section_select = '';
		$message = '';
		if (isset($_SESSION['ticket_form_fields']) && $_SESSION['ticket_form_fields']) {
			foreach($_SESSION['ticket_form_fields'] as $name => $value)
				try {
					if ($name == 'section')
						$section_select = $value;
					elseif ($name == 'char')
						$char_select = $value;
					elseif ($name == 'message')
						$message = $value;
					$form->setData($name, $value);
				} catch (Exception $ex) {}
		}

		if (!$message) {
			$prefill_message = _t($ticket_form.'_prefill');
			if ($prefill_message[0] != '_')
				$form->setData('message', $prefill_message);
		}

		$form->setData('channel', $channel);
		
		$datasource = new jFormsStaticDatasource();
	//	$datasource->data = $this->form_channels;
		$datasource->data =  [
			'' => _t('ticket_section'),
			'bugs' => _t('technical_general'),
			'crash' => _t('technical_crash'),
			'missions' => _t('technical_missions'),
			'patch' => _t('technical_patch'),
		];
		
		if ($section_select)
			$section_select = array_search($section_select, array_keys($datasource->data));
		
		$control = $form->getControl('section');
		if ($control)
				$control->datasource = $datasource;

/*			<item value="" locale="main.ticket_section"></item>
	<item value="bugs" locale="">xx</item>
	<item value="crash">My client crash</item>
	<item value="missions">Issues with Missions</item>
	<item value="patch">Issues with latest patch</item>
		
		*/

		$tpl->assign('channels', $this->form_channels);
		$tpl->assign('channel', $channel);
		if ($help != '---')
			$tpl->assign('help', '<div class="information">'.$help.'</div>');
		else
			$tpl->assign('help', '');
		$tpl->assign('notes', $notes);
		$tpl->assign('infos', $infos);
		$tpl->assign('search_form', null);
		$tpl->assign('ticket_form', $form);
		$tpl->assign('errors', $this->param('errors'));
		if ($this->auth->isLogged()) {
			$tpl->assign('logged', true);
			$tpl->assign('ig', $this->user->ig);
			$user = $this->show_user;
			$user_infos = $user->getBrief();
			$chars = [];
			foreach($user_infos['characters'] as $char) {
				if ($char)
					$chars[$char] = $char;
			}
			
			$tpl->assign('chars', $chars);
			$datasource = new jFormsStaticDatasource();
			$datasource->data = ['' => '-- '._t('select_char').' --']+$chars+['_another' => _t('ticket-another-char')];
			$char_select = $char_select == '_another'?'1':'nil';
			if ($this->user->ig && $channel != 'intro')
				$form->setData('char',  $this->param('name'));
		
			$control = $form->getControl('char');
			if ($control)
				$control->datasource = $datasource;
			
			$datasource = new jFormsStaticDatasource();
			$datasource->data = ['' => '-- '._t('select_char').' --']+$chars;
			$control = $form->getControl('slotA');
			if ($control)
				$control->datasource = $datasource;
		} else {
			$tpl->assign('logged', false);
			$tpl->assign('ig', false);
		}
		
		$tpl->assign('char_select', $char_select);
		$tpl->assign('section_select', $section_select);
		
		$channels = new Support\Channels($this->ini, $this->user);
		$channels = $this->channels->getForMenu($this->auth->isLogged(), $this->params());
		$rep->body->assign('channels', $channels);

		$rep->body->assign('MAIN', $tpl->fetch('new_ticket'));
		Ry\Common::setupMessage($rep);
		return $rep;
	}

	public function add() {
		$rep = $this->getResponse('redirect');

		$ticket_form = $this->param('channel')?'ticket_'.$this->param('channel'):'ticket';
	
		$wkr = new jWiki('markdown_to_xhtml');
		$wkrt = new jWiki('markdown_to_text');
		
		$form = jForms::fill($ticket_form);
		$form->setData('creation_date', Ry\DB::today());
		$form->setData('update_date', Ry\DB::today());
		$token = '';
		$email = '';
		$force_channel = '';
		$form->setData('type', 'new');
		if ($this->auth->isLogged()) {
			$form->setData('user_id', $this->user->id);
			$form->setData('lang', $this->user->lang);
			$form->setData('contact', ucfirst($this->user->name));
			$form->setData('email', 'me@ryzom.com');
		} else {
			$form->setData('user_id', 0);
			$form->setData('lang', \Ry\Common::getUserLang());
			$email = $this->param('email');
			$token = \Ry\Account::getHashedPassword($email);
			$form->setData('contact', $email);
			$form->setData('email', $email);
			$form->setData('mail_id', $token);
			$form->setData('char', '?');
			if ($this->param('channel') == 'Others')
				$force_channel = 'Emails';
			jMessage::add(_t('ticket_waiting'));
		}

		if ($form->check()) {
			$fields = $this->params();

			if ($fields['char'] == '_another')
				$fields['char'] = $this->param('for_char');
			//	$form->setData('char', $this->param('for_char'));

			$subject = $this->param('subject');
			$content = $this->param('message');
				
			if ($ticket_form != 'ticket') {
				$tpl = new jTpl();
				$subject = _t($ticket_form.'_subject');
				foreach($fields as $name => $value) {
					if ($name == 'subject' || $name == 'message')
						$value = $wkrt->render(htmlspecialchars($value));
					$tpl->assign($name, $value);
					$subject = str_replace('$'.$name, ucfirst($value), $subject);
				}
				$content = $tpl->fetch($ticket_form);
			}
			
			foreach($this->form_channels as $form_channel) {
				if ($form_channel[0] == $this->param('channel')) {
					$channel = $form_channel[3];
				}
			}
			
			if ($force_channel)
				$channel = $force_channel;

			$form->setData('channel', $channel);				
			$raw_message = $wkr->render($content);
			$form->setData('message', $raw_message);
			$form->setData('subject', $wkr->render($subject));

			$ticket_id = strval($form->saveToDao('support~ryzocket__tickets', null, 'ryzocket'));
			$sticket = sprintf('%09d', $ticket_id);
			$screens = json_decode($fields['attachments'], true);
			$folder = $sticket[strlen($sticket)-3].'/'.$sticket[strlen($sticket)-2].'/'.$sticket[strlen($sticket)-1];
			if (!file_exists(jApp::wwwPath().'/uploads/screens/'.$folder))
				mkdir(jApp::wwwPath().'/uploads/screens/'.$folder, 0777, true);
			if ($screens) {
				foreach($screens as $screen)
					rename(jApp::wwwPath().'/uploads/screens/0/0/0/'.$screen, jApp::wwwPath().'/uploads/screens/'.$folder.'/'.$screen);
			}

			if ($token || $email)
				$url = jUrl::getFull('support~ticket:index', ['ticket_id' => $ticket_id, 'token' => $token, 'email' => $email]);
			else
				$url = jUrl::getFull('support~ticket:index', ['ticket_id' => $ticket_id]);
			$message = '<a style="color: orange" href="'.$url.'">'.$url.'</a>';
			$lang = $user->infos->Community;

			$subject = html_entity_decode($subject); 

			$rocket_channel = $this->channels->getChannel($channel);
			if ($rocket_channel)
				$this->utils->sendRocket($subject, $rocket_channel->rocketchat_channel, 'New TS / '.$channel, ':mega:', [
				'title' => 'Ticket #'.$ticket_id.': '.str_replace(['https:', 'http:'], 'URL:', $subject),
				'title_link' => jUrl::getFull('support~ticket:index', ['ticket_id' => $ticket_id]),
				'text' => $wkrt->render(str_replace(['https://', 'http://'], 'URL: ', $content)),
				'pretext' => '',
				'color' => 'lightgreen']);

			$subject = _t('we_receive_your_ticket', [$ticket_id, $subject]);
			$urlig = 'https://apps.ryzom.com/support/ticket?ticket_id='.$ticket_id;
			
			
			
			$this->utils->sendEmailWithTemplate(' '.$subject,
					'api~mail_content_generic',
					$message, 
					$values=[
						'bgcolor' => 'orange',
						'color' => 'black',
						'infos' => $subject.'<br/>'._t('this_is_an_automatic_email'),
						'text' => $message],
					$email?$email:$this->user->id);
			$this->utils->sendIGEmail('#'.$this->user->id, $subject, '<a style="color: orange" href="'.$urlig.'">'.$urlig.'</a>', 'Ryzom Support');
			$rep->action = 'support~channel:index';
			//$rep->params = ['name' => $channel];//, 'token' => $token, 'email' => $email];
			$_SESSION['ticket_form_fields'] = [];
		} else {
			$rep->action = 'support~ticket:create';
			$_SESSION['ticket_form_fields'] = $this->params();
			$rep->params = ['channel' => $this->param('channel'), 'errors' => $form->getErrors()];
		}
		Ry\Common::setupMessage($rep);
		return $rep;
	}

	public function close() {
		$rep = $this->getResponse('htmlfragment');
		$db = Ry\DB::spawn('support~ryzocket');
		
		$show_user = $this->param('show_user');
		$ticket_id = $this->param('ticket_id');
		$guest_email = $this->param('email');
		$guest_token = $this->param('token');
		$is_agent = $this->user->checkAccess('support:all');
		$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
		if ($this->channels->canAccessChannel($ticket->channel)
			||
			($ticket->user_id != 0 && $this->user->id == $ticket->user_id)
			||
			($guest_email && $ticket->email == $guest_email && $ticket->mail_id == $guest_token)) {
			$db->update('tickets', ['update_date' => Ry\DB::today(), 'type' => 'closed'], ['id' => $ticket_id]);
			$db->insert('tickets_notes', ['subject' => 'set_status', 'parent_id' => $ticket_id, 'user_id' => $this->user->id, 'contact' => ($this->user->id?$this->user->name:$ticket->email), 'message' => 'closed', 'sender' => $is_agent?'agent':'user', 'type' => 'action', 'creation_date' => Ry\DB::today()]);
		}

		if ($this->show_user_id)
			$this->redirect(jUrl::get('support~ticket:index', ['show_user' => $this->show_user_id, 'ticket_id' => $ticket_id, 'email' => $guest_email, 'token' => $guest_token]));
		else
			$this->redirect(jUrl::get('support~channel:index', ['ticket_id' => $ticket_id, 'email' => $guest_email, 'token' => $guest_token]));
	}
	
	public function open() {
		$rep = $this->getResponse('htmlfragment');
		$db = Ry\DB::spawn('support~ryzocket');
		
		$show_user = $this->param('show_user');
		$ticket_id = $this->param('ticket_id');
		$is_agent = $this->user->checkAccess('support:all');

		$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
		if ($this->channels->canAccessChannel($ticket->channel)) {
			$db->update('tickets', ['update_date' => Ry\DB::today(), 'type' => 'open'], ['id' => $ticket_id]);
			$db->insert('tickets_notes', ['subject' => 'set_status', 'parent_id' => $ticket_id, 'user_id' => $this->user->id, 'contact' => $this->user->name, 'message' => 'wip', 'sender' => $is_agent?'agent':'user', 'type' => 'action', 'creation_date' => Ry\DB::today()]);
		}
		
		if ($this->show_user_id)
			$this->redirect(jUrl::get('support~ticket:index', ['show_user' => $this->show_user_id, 'ticket_id' => $ticket_id, 'email' => $guest_email, 'token' => $guest_token]));
		else
			$this->redirect(jUrl::get('support~channel:index', ['ticket_id' => $ticket_id, 'email' => $guest_email, 'token' => $guest_token]));

	}

	public function changeChannel() {
		$rep = $this->getResponse('htmlfragment');
		$db = Ry\DB::spawn('support~ryzocket');
		
		$show_user = $this->param('show_user');
		$ticket_id = $this->param('ticket_id');
		$channel = $this->param('channel');
		
		$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
		if ($this->channels->canAccessChannel($ticket->channel)) {
			$db->update('tickets', ['channel' => $channel], ['id' => $ticket_id]);
			$db->insert('tickets_notes', ['subject' => 'channel_to', 'parent_id' => $ticket_id, 'user_id' => $this->user->id, 'contact' => $this->user->name, 'message' => $channel, 'sender' => 'agent', 'type' => 'action', 'creation_date' => Ry\DB::today()]);
		}
		$this->redirect(jUrl::getFull('support~ticket:index', ['show_user' => $show_user, 'ticket_id' => $ticket_id]));
	}
	
	public function changeAgent() {
		$rep = $this->getResponse('htmlfragment');
		$db = Ry\DB::spawn('support~ryzocket');
		
		$show_user = $this->param('show_user');
		$ticket_id = $this->param('ticket_id');
		$agent = $this->param('agent');
		
		$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
		if ($this->channels->canAccessChannel($ticket->channel)) {
			$params = ['assignee' => $agent];
			if ($ticket->type == 'new')
				$params['type'] = 'wip';
			$db->update('tickets', $params, ['id' => $ticket_id]);
			$db->insert('tickets_notes', ['subject' => 'assigned_to', 'parent_id' => $ticket_id, 'user_id' => $this->user->id, 'contact' => $this->user->name, 'message' => $agent, 'sender' => 'agent', 'type' => 'action', 'creation_date' => Ry\DB::today()]);
		}
		$this->redirect(jUrl::getFull('support~ticket:index', ['show_user' => $show_user, 'ticket_id' => $ticket_id]));
	}
	
	public function changeLang() {
		$rep = $this->getResponse('htmlfragment');
		$db = Ry\DB::spawn('support~ryzocket');
		
		$show_user = $this->param('show_user');
		$ticket_id = $this->param('ticket_id');
		$lang = $this->param('ticket_lang');
		
		$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
		if ($this->channels->canAccessChannel($ticket->channel)) {
			$params = ['lang' => $lang];
			$db->update('tickets', $params, ['id' => $ticket_id]);
		}
		$this->redirect(jUrl::getFull('support~ticket:index', ['show_user' => $show_user, 'ticket_id' => $ticket_id]));
	}
	
	public function deeplize() {
		$rep = $this->getResponse('htmlfragment');
		$db = Ry\DB::spawn('support~ryzocket');
		
		$show_user = $this->param('show_user');
		$ticket_id = $this->param('ticket_id');
		$note_id = $this->param('note_id');
		$dst_lang = $this->param('dst_lang');
		
		$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
		if ($this->channels->canAccessChannel($ticket->channel)) {
			if ($note_id) {
				$note = $db->getSingle('tickets_notes', ['id' => $note_id]);
				$translator = new \DeepL\Translator($this->ini['rocketchat']['deepl_api']);
				$result = $translator->translateText($note->message, null, $dst_lang=='en'?'en-GB':$dst_lang);
				$text = $result->text;
				$params = [$dst_lang => $text];
				$db->update('tickets_notes', $params, ['id' => $note_id]);
			} else {
				$translator = new \DeepL\Translator($this->ini['rocketchat']['deepl_api']);
				$result = $translator->translateText($ticket->message, null, $dst_lang=='en'?'en-GB':$dst_lang);
				$text = $result->text;
				$params = [$dst_lang => $text];
				$db->update('tickets', $params, ['id' => $ticket_id]);
			}
		}
		$this->redirect(jUrl::getFull('support~ticket:index', ['show_user' => $show_user, 'ticket_id' => $ticket_id]));
	}
	
	
	public function upload() {
		$rep = $this->getResponse('htmlfragment');
		$ticket_id = $this->param('ticket', 0);
		$sticket = sprintf('%09d', $ticket_id);
		$folder = $sticket[strlen($sticket)-3].'/'.$sticket[strlen($sticket)-2].'/'.$sticket[strlen($sticket)-1];
		$random = bin2hex(random_bytes(10));
		$screen = preg_replace('/[^._a-z0-9]+/', '-', strtolower($_FILES['screen']['name']));
		$file = $random.'_'.$screen;
		mkdir(jApp::wwwPath().'/uploads/screens/'.$folder, 0777, true);
		if (!empty($_FILES)) {
			if ($ticket_id) {
				$db = Ry\DB::spawn('support~ryzocket');
				$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
				$screens = json_decode($ticket->attachments, true);
				$screens[] = $file;
				$db->update('tickets', ['attachments' => json_encode($screens)], ['id' => $ticket_id]);
				$db->insert('tickets_notes', ['subject' => 'add_screen', 'parent_id' => $ticket_id, 'user_id' => $this->user->id, 'contact' => $this->user->name, 'message' => $file, 'sender' => 'agent', 'type' => 'screen', 'creation_date' => Ry\DB::today()]);
			}
			move_uploaded_file($_FILES['screen']['tmp_name'], jApp::wwwPath().'/uploads/screens/'.$folder.'/'.$file);
			$rep->addContent($file);
		}
		return $rep;
	}
	
	public function uploadDelete() {
		$rep = $this->getResponse('htmlfragment');
		$ticket_id = $this->param('ticket');
		$screen = preg_replace('/[^._a-z0-9]+/', '-', strtolower($this->param('screen')));
		if ($ticket_id) {
			$db = Ry\DB::spawn('support~ryzocket');
			$ticket = $db->getSingle('tickets', ['id' => $ticket_id]);
			$screens = json_decode($ticket->attachments, true);
			if (($key = array_search($screen, $screens)) !== false)
				unset($screens[$key]);
			$sticket = sprintf('%09d', $ticket_id);
			$folder = $sticket[strlen($sticket)-3].'/'.$sticket[strlen($sticket)-2].'/'.$sticket[strlen($sticket)-1];
			$db->update('tickets', ['attachments' => json_encode($screens)], ['id' => $ticket_id]);
			$db->deleteBy('tickets_notes', ['parent_id' => $ticket_id, 'type' => 'screen', 'message' => $screen]);
			unlink(jApp::wwwPath().'/uploads/screens/'.$folder.'/'.$screen);
		} else
			unlink(jApp::wwwPath().'/uploads/screens/0/0/0/'.$screen);
		return $rep;
	}
}

