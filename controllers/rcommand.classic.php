<?php
/**
* @package   me
* @subpackage support
* @author    me.ryzom.com
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/

class rcommandCtrl extends Ry\UserController {
	
	function index() {
		$params = explode(' ', $this->param('text'));
		if ($params[0][0] == '/')
			$params[0] = substr($params[0], 1);
		else
			array_shift($params); // remove cs

		$action = array_shift($params);
		
		switch($action) {
			case 'bug':
				return $this->_createGitlabIssue($params);
		}
		
		return $this->_answer('Error');
	}
	
	private function _createGitlabIssue($params) {
		if (!in_array(strtolower($params[0]), ['ryztart', 'daily_missions'])) {
			return $this->_answer('Missing category !\nSyntax are: /bot <category> <title>', ':exclamation:', 'red');
		}
	}
	
	private function _answer($text, $emoji='', $color='#B4D6D5')
	{
		$rep = $this->getResponse('json');
		$rep->data = '{"icon_emoji": "'.$emoji.'","attachments": [{"text": "'.$text.'","color": "'.$color.'"}]}';
		
		return $rep;
	}
}

