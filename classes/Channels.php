<?php
namespace Support;
use Ry;

class Channels {
	private $ini = NULL;
	private $user = NULL;
	
	public function __construct($ini, $user) {
		$this->ini = $ini;
		$this->user = $user;
		$db = Ry\DB::spawn('support~ryzocket');
		$channels = $db->findAll('channels');
		$this->channels = [];
		foreach($channels as $channel)
			$this->channels[$channel->name] = $channel;
	}
	
	function getChannel($name) {
		if (isset($this->channels[$name]))
			return $this->channels[$name];
		return Null;
	}
	
	public function getFormChannels() { 
		foreach($this->channels as $name => $channel) {
			if ($channel->form_name) {
				if ($this->user->id != 0 || $channel->form_anonyme == '1')
					$ret[$channel->form_id] = [$channel->form_name, _t('channel_'.$channel->form_name), $channel->icon, $channel->name];
			}
		}
		unset($ret['']);
		ksort($ret);
		return $ret;
	}
	
	public function getUserChannels() {
		$valid_channels = [];
		$query_channels = [];
		foreach($this->channels as $channel) {
			if ($this->user->inGroup($channel->roles)) {
				if ($channel->name != 'Player Tickets' || $this->user->show_user_id)
					$valid_channels[] = $channel;
				if ($channel->name != 'My Tickets' && $channel->name != '*NEW*')
					$query_channels[] = '\''.$channel->name.'\'';
			}
		}
		$query_channels = implode(',', $query_channels);
		return [$valid_channels, $query_channels];
	}
	
	public function canAccessChannel($channel) {
		return $this->user->inGroup($this->channels[$channel]->roles);
	}
	
	public function getChannelAgents($channel) {
		$db_nel = Ry\DB::spawn('api~nel');
		$db_groups = Ry\DB::spawn('admin~nel');
		$agents = ['' => '&lt;Not Assigned&gt;'];
		$roles = explode(':', $this->channels[$channel]->roles);
		p($roles, 'roles');
		foreach($roles as $role) {
			if (!isset($agents[$role])) {
				$list_agents = '';
				$agents_in_role = $db_nel->get('user', ['ToolsGroup' => 1, '!GroupName' => 'yubo', 'Privilege:LIKE' => '%:'.$role.':%']);
				foreach($agents_in_role as $agent_in_role) {
					$agents[$agent_in_role->UId] = ucfirst($agent_in_role->Login);
					$list_agents .= ucfirst($agent_in_role->Login).' ';
				}
				/*
				$agents_in_role = $db_nel->get('user', ['ToolsGroup' => 1, '!GroupName' => 'yubo', 'ExtendedPrivilege:LIKE' => '%:'.$role.':%']);
				foreach($agents_in_role as $agent_in_role) {
					$agents[$agent_in_role->UId] = ucfirst($agent_in_role->Login);
					$list_agents .= ucfirst($agent_in_role->Login).' ';
				}
				*/
				
				$agents_in_role = $db_groups->get('user_groups', ['groupname' => $role]);
				foreach($agents_in_role as $agent_in_role) {
					$agent = $db_nel->getSingle('user', ['ToolsGroup' => 1, '!GroupName' => 'yubo', 'Login' => $agent_in_role->username]);
					if ($agent) {
						$agents[$agent->UId] = ucfirst($agent->Login);
						$list_agents .= ucfirst($agent->Login).' ';
					}
				}
			}
		}
		asort($agents);
		return $agents;
	}
	
	public function getForMenu($logged=true, $params=[]) {
		list($valid_channels, $query_channels) = $this->getUserChannels();
		$final_channels = [];
		$empty_channels = [];
		$remove_player_tickets = false; 
		$total = $new = [];
		foreach($valid_channels as $i => $channel) {
			if ($channel->name == 'My Tickets') {
				if ($this->user->checkAccess('support:all')) {
					$total = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE assignee = %s AND type IN ("waiting", "done", "new", "open", "wip")', $this->user->char_name);
					$new = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE assignee = %s AND type IN ("wip", "new")', $this->user->char_name);
				} else {
					if ($logged) {
						$total = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE user_id = %s AND type IN ("new", "open", "wip")', $this->user->id);
						$new = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE user_id = %s AND type = "new"', $this->user->id);
					} else if (isset($params['email']) && $params['email'] && isset($params['token']) && $params['token']) {
						$total = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE email = %s AND mail_id = %s AND type IN ("new", "open", "wip", "done" ,"waiting")', [$params['email'], $params['token']]);
						$new = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE email = %s AND mail_id = %s AND type IN ("new", "done")', [$params['email'], $params['token']]);
					}
				}
			} elseif ($channel->name == 'Player Tickets') {
				if ($this->user->show_user_id && is_numeric($this->user->show_user_id)) {
					$total = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE user_id = %s AND type IN ("new", "open", "wip")', $this->user->show_user_id);
					$new = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE user_id = %s AND type = "new"', $this->user->show_user_id);
					$channel->name = '- '.$this->user->show_user->name;
				} else {
					$remove_player_tickets = $i;
					continue;
				}
			} elseif ($channel->name == '*NEW*') {
				if (!$this->user->checkAccess('support:all'))
					continue;
				$total = $new = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE type = "new" AND channel IN ('.$query_channels.')');
			} else {
				$total = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE channel = %s AND type != "spam" AND type != "closed" AND type != "waiting"', $channel->name);
				$new = (array)Ry\DB::querySingle('ryzocket', 'SELECT count(*) FROM tickets WHERE channel = %s AND ( type = "new" OR type = "wip")', $channel->name);
			}

			$channel->tickets = str_replace(' ', '&nbsp;', sprintf('%d', $total['count(*)']));
			$channel->new_tickets = str_replace(' ', '&nbsp;', sprintf('%d', $new['count(*)']));
			$channel->have_tickets = $total['count(*)'] + $new['count(*)'] != 0;
			if ($new['count(*)'])
				$channel->color = 'lightgreen';
			else
				$channel->color = 'grey';
			
			if ($total['count(*)']  || $channel->name == 'My Tickets')
				$final_channels[] = $channel;
			else 
				$empty_channels[] = $channel;
		}
		
		if ($remove_player_tickets !== false) {
			unset($final_channels[$remove_player_tickets]);
			unset($empty_channels[$remove_player_tickets]);
		}
		return array_merge($final_channels, $empty_channels);
	}
}
