{if $ig}
<style>
{literal}
	* {
		font-size: 14px;
	}
	
	.ticket {
		font-size: 13px;
	}
	
	.ticket-note-message {
		font-size: 13px;
	}
	
{/literal}
</style>
{/if}


{if $ig}
	<table width="100%">
	<td colspan="2" valign="middle" bgcolor="#555" height="32px"><table width="100%" cellspacing="0" cellpadding="5" ><tr><td>
{/if}
<div class="bg-green" id="new-ticket"><span id="new-ticket-title">{t 'ticket_list'}</span>

{if $ig}
	</td>
	<td width="32px" bgcolor="#222"><a href="{jurl 'support~channel:index'}">{i '32/text_list_bullets'}</a></td>
	<td width="150px" bgcolor="#222"><a style="text-decoration: none; font-weight: bold" href="{jurl 'support~ticket:create'}">{t 'new_ticket'}</a></td>
	</tr></table></td></tr></table>
	<table width="100%">
		<tr style="background-color:pink; color: black; font-weight: bold"><td align="center">{t 'message_ticket_spam'}</td></tr>
	</table>
{else}
	<div id="support-search" class="">
{/if}

{if $tickets}
	<table width="100%" style="max-width: 1024px" cellspacing="0">
			<tr>
				<th width="16px" height="32px"></th>
				{foreach $fields as $total_fields => $field}
					{if $field == 'id'}
					<th align="center">Ticket&nbsp;ID</th>
					{else}
					<th style="padding: 0 10px" align="center">{t 'ticket_'.$field}</th>
					{/if}
				{/foreach}
				<th width="64px"></th>
			</tr>

			{assign $i=1}
			{foreach $tickets as $ticket}
			{if $ticket == "---"}
				<tr><td colspan="{$total_fields+3}" align="center">&nbsp;</td></tr>
				<tr><td style="background-color: orange; color: black; font-weight: bold" colspan="{$total_fields+3}" align="center">Latest 10 Closed tickets</td></tr>
			{else}
			<tr class="row{$i++ % 2} ticket ticket-{$ticket->type} {if !$agent && $ticket->type == 'closed'}grayed{/if}"onclick="openUrl('/support/ticket/?ticket_id={$ticket->id}&show_user={if $ticket->user_id != 0}{$ticket->user_id}{else}{$ticket->email}{/if}&{if $email}email={$email}&{/if}{if $token}token={$token}&{/if}'); return false;">
				<td width="16px" align="center">
					{if $agent}
						{$icons[$ticket->type]}
					{else}
						{$icons_user[$ticket->type]}
					{/if}
				</td>
				{foreach $fields as $field}
					{if $field == 'assignee'}
						{if $ticket->assignee}
							<td class="ticket-agent" align="center" style="width: 32px;"><div style="width: 32px; margin: auto"><img src="https://chat.ryzom.com/avatar/{$ticket->assignee}" width="32px" height="32px" title="{$ticket->assignee}" /><br /><label>{$ticket->assignee}</label></div></td>
						{else}
							<td class="ticket-agent" align="center" style="width: 32px;"><img src="https://app.ryzom.com/data/icons/32/status_away.png" style="vertical-align: middle" width="32px" height="32px" /></td>
						{/if}
					{elseif $field == 'channel'}
						{if $ticket->channel}
							<td class="ticket-channel" align="left" width="100px">{i '16/'.$channels[$ticket->channel]->icon} {$ticket->$field}</td>
						{else}
							<td class="ticket-channel" align="left" width="100px">{i '16/help'} {$ticket->$field}</td>
						{/if}
					{elseif $field == 'contact'}
					<td class="ticket-contact"  align="center">
							{if $sgm}{$ticket->contact}<br />{/if}{if $ticket->char != "?"}{if $ticket->char == "_another"}<span class="white">*&nbsp;Another&nbsp;Char&nbsp;*</span>{else}<span style="color: #BDE3C1">{$ticket->char}</span>{/if}{/if}
						</td>
					{else}
						<td class="ticket-{$field}{if $field == 'subject'} truncate-overflow{/if}"  align="center">
							{$ticket->$field}
						</td>
					{/if}
				{/foreach}
				<td align="center">
					{if $ig}
						<a href="{jurl 'support~ticket:index'}?ticket_id={$ticket->id}#last">{i '32/page_go', 'Open'}</a>
					{else}
						<a class="ticket-open-window" href="#" onclick="openWindow('/support/ticket/?ticket_id={$ticket->id}&show_user={if $ticket->user_id != 0}{$ticket->user_id}{else}{$ticket->email}{/if}&{if $email}email={$email}&{/if}{if $token}token={$token}&{/if}'); event.stopPropagation(); return false;"><i class="far fa-window-restore"></i></a>
					{/if}
				</td>
			</tr>
			{/if}
			{/foreach}
		</table>
	</div>
{/if}
