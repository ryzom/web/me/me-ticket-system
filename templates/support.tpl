{if in_array('account:search', $user->access)}
<div class="box-grey account">
	{form $form, 'support~index'}
	<fieldset>
		{assign $transparency = "30"}
		{formcontrols}
			{if $transparency == "30"}
				{assign $transparency = "00"}
			{else}
				{assign $transparency = "30"}
			{/if}
			
			{ifctrl 'all'}
			<div class="padding-5 float-left slim75">
			<strong>{ctrl_label 'all'}</strong> {ctrl_control 'all', ['class' => 'support_search_all']} <button class="badge-secondary padding-5 margin-5 round" onclick="postFormAjax('{ryurl 'support~default:search'}', 'support_search_result', 'jforms_support_search_account'); return false;">Search</button>  <button class="badge-red white padding-5 margin-5 round" onclick="runAjax('{ryurl 'support~default:search'}', 'support_search_result'); return false;">Clear</button>
			</div>
			{else}
				{ifctrl 'UId'}
					<button class="badge-primary float-right padding-5 margin-5 round" onclick="switchHidden('support_search_extended_options'); return false;">Show/Hide All Fields</button>
					<div id="support_search_extended_options" class="hidden">
					<table width="100%">
				{/ifctrl}
					<tr style="background-color: #000000{$transparency}"><td><strong>{ctrl_label}</strong></td><td>{ctrl_control}</td></tr>
				{ifctrl 'Country'}
					<tr><td></td><td><button class="badge-secondary float-right padding-5 margin-5 round" onclick="postFormAjax('{ryurl 'support~default:search'}', 'support_search_result', 'jforms_support_search_account'); return false;">Search</button></td></tr>
					</table>
					</div>
				{/ifctrl}
			{/ifctrl}
		{/formcontrols}
	</fieldset>
	{/form}
</div>
<div id="support_search_result"></div>
{/if}
