{foreach $channels as $channel}
	<li class="nav-sub-item"> 
		<a href="/support/channel/?name={$channel->name}{if $show_user_id}&show_user={$show_user_id}{/if}{if $email}&email={$email}{/if}{if $token}&token={$token}{/if}" class="nav-sub-link{if isset($selected_channel) &&  $channel->name == $selected_channel} selected{/if}">
		{i '16/'.$channel->icon}
		{$channel->name}{if isset($channel->tickets) && $channel->tickets}<span class="float-right {$channel->color}">{$channel->new_tickets}/{$channel->tickets}</span>{/if}
		</a>
	</li>
{/foreach}
<script>
	let Url = "{jurl 'support~ticket:getMenu', array('selected_channel' => $selected_channel, 'show_user' => $show_user_id, 'email' => $email, 'token' => $token)}"
	{literal}
	function updateMenu() {
		runAjax(Url.replaceAll('&amp;', '&'), 'menu_support', true);
	}
	
	window.setInterval(function() { updateMenu(); }, 60000);
	{/literal}
</script>