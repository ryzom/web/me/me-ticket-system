{allow account:search}
<div class="width-50 float-left  padding-5">
	{form $search_user_form, 'support~search:user'}
	<fieldset class="box-grey">
		{assign $transparency = "30"}
		{formcontrols}
			{if $transparency == "30"}
				{assign $transparency = "00"}
			{else}
				{assign $transparency = "30"}
			{/if}
			
			{ifctrl 'all'}
			<div class="padding-5">
				<strong>{ctrl_label 'all'}</strong> {ctrl_control 'all', ['class' => 'support_search_all slim75']}
			</div>
			{else}
				{ifctrl 'UId'}
					<button class="badge-secondary float-right padding-5 margin-5 round" onclick="postFormAjax('{ryurl 'support~search:user'}', 'support_search_result', 'jforms_support_search_account'); hide('ticket_search_extended_options');  hide('user_search_extended_options'); return false;">Search</button>
					<a class="badge-primary margin-5 float-left padding-5 round button" onclick="switchHidden('user_search_extended_options'); return false;">All Options</a>
					<div id="user_search_extended_options">
					<script>switchHidden('user_search_extended_options');</script>
					<table width="100%">
				{/ifctrl}
					<tr style="background-color: #000000{$transparency}"><td><strong>{ctrl_label}</strong></td><td>{ctrl_control}</td></tr>
				{ifctrl 'Country'}
					<tr><td></td><td>
						<button class="badge-secondary float-right padding-5 margin-5 round" onclick="postFormAjax('{ryurl 'support~search:user'}', 'support_search_result', 'jforms_support_search_account'); hide('ticket_search_extended_options');  hide('user_search_extended_options'); return false;">Search</button>
						</td></tr>
					</table>
					</div>
				{/ifctrl}
			{/ifctrl}
		{/formcontrols}
	</fieldset>
	{/form}
</div>
{/allow}
<div class="float-left width-50 padding-5">
	{form $search_ticket_form, 'support~search:ticket'}
	<fieldset class="box-grey">
		{assign $transparency = "30"}
		{formcontrols}
			{if $transparency == "30"}
				{assign $transparency = "00"}
			{else}
				{assign $transparency = "30"}
			{/if}
			
			{ifctrl 'all'}
			<div class="padding-5">
			<strong>{ctrl_label 'all'}</strong> {ctrl_control 'all', ['class' => 'support_search_all slim75']}
			</div>
			{else}
				{ifctrl 'Id'}
					<button class="badge-secondary float-right padding-5 margin-5 round" onclick="postFormAjax('{ryurl 'support~search:ticket'}', 'support_search_result', 'jforms_support_search_ticket'); return false;">Search</button>
					<a class="badge-primary margin-5 padding-5 round button" onclick="switchHidden('ticket_search_extended_options'); return false;">All Options</a>
					<div id="ticket_search_extended_options">
					<script>switchHidden('ticket_search_extended_options');</script>
					<table width="100%">
				{/ifctrl}
				{ifctrl 'Type'}
					<tr style="background-color: #000000{$transparency}"><td><strong>{ctrl_label}</strong></td><td id="support-search-type">{ctrl_control}</td></tr>
					<tr><td></td><td>
						<button class="badge-secondary float-right padding-5 margin-5 round" onclick="postFormAjax('{ryurl 'support~search:ticket'}', 'support_search_result', 'jforms_support_search_ticket'); switchHidden('ticket_search_extended_options'); return false;">Search</button>
						</td></tr>
					</table>
					</div>
				{else}
					<tr style="background-color: #000000{$transparency}"><td colspan="2"> <div><table><tr><td width="130px"><strong>{ctrl_label}</strong></td><td>{ctrl_control}</td></tr></table></div></td></tr>
				{/ifctrl}
			{/ifctrl}
		{/formcontrols}
	</fieldset>
	{/form}
</div>
<div id="support_search_result">{$search_saved_results}</div>
