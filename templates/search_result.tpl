{if $results}
	<div id="support-search" class="">
		<table width="100%">
			<tr>
				{foreach $categories as $cat}
					<th align="center">{$cat}</th>
				{/foreach}
			</tr>
			{assign $i=1}
			{foreach $results as $result}
				<tr class="row{$i % 2}">
					{assign $i=$i+1}
					{assign $j=0}
					{foreach $result as $field}
						{if $categories[$j] == 'UId'}
							<td align="center"><a href="/?show_user={$field}">{$field}</a></td>
						{elseif $categories[$j] == 'id'}
							<td align="center"><a href="/support/ticket?ticket_id={$field}&show_user={$result->contact}" target="_blank">{$field}</a></td>
						{else}
							<td align="center">{$field}</td>
						{/if}
					{assign $j=$j+1}
					{/foreach}
				</tr>
			{/foreach}
		</table>
	</div>
	{if $total > 100}
		<br><font color='red'><strong>Only 100 results displayed over a total of {$total}, please be more specific...</strong></font>
	{/if}
{/if}