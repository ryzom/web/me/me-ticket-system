{if in_array('account:search', $user->access)}
<h1>Send email to <span class="purple">{$player_name}</span></h1>
<div class="box-grey account">
	{formfull $form, 'support~default:sendmail', array('show_user' => $show_user_id)}
</div>
<div id="support_search_result"></div>
{/if}
