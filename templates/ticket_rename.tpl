[SGM]
This user request a change of name.
Propositions:
1. {$name_prop1} [SGM: Accept this rename](#)
2. {$name_prop2} [SGM: Accept this rename](#)
3. {$name_prop3} [SGM: Accept this rename](#)

{$message}
[SGM][PLAYER]
Request of a change of name.
Propositions:
1. {$name_prop1}
2. {$name_prop2}
3. {$name_prop3}

{$message}
[PLAYER]
