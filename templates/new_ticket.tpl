{if $ticket_form}
{if $ig}
<style>
{literal}
	* {
		font-size: 14px;
	}

{/literal}
</style>

<table width="100%"><tr>
	<td colspan="2" valign="middle" bgcolor="#555" height="32px">
		<table width="100%" cellspacing="0" cellpadding="5" ><tr>
			<td>
{/if}
<div class="bg-green" id="new-ticket"><span id="new-ticket-title">{t 'new_ticket'}</span>
	
{if $ig}
	</td>
	<td width="32px" bgcolor="#222"><a href="{jurl 'support~channel:index'}">{i '32/text_list_bullets'}</a></td>
	<td width="150px" bgcolor="#222"><a style="text-decoration: none; font-weight: bold" href="{jurl 'support~channel:index'}">{t 'back_to_ticket_list'}</a></td>
	</tr></table></td></tr>
	<tr><td width="270px" valign="top" bgcolor="#222">
{/if}
<div class="h"> 
	<div class="v" id="new-ticket-menu" >
		{if $ig}<table width="100%" cellspacing="0"><tr><td height="50px" colspan="2" align="center" bgcolor="#333">{/if}
		<div class="inline-block padding-10 orange bg-gray">{t 'select_category'}</div>
		{if $ig}
		</td></tr>
		{foreach $channels as $menu_channel}
			<tr bgcolor="{if $channel==$menu_channel[0]}#000000AA{else}#00000000{/if}"><td height="50px" width="42px"><a href="{jurl 'support~ticket:create'}?channel={$menu_channel[0]}">{i '32/'.$menu_channel[2]}</a></td><td><a href="{jurl 'support~ticket:create'}?channel={$menu_channel[0]}">{$menu_channel[1]}</a></td></tr>
		{/foreach}
		</table>
		{else}
		{foreach $channels as $menu_channel}
			<div title="{$menu_channel[1]}" class="inline-block padding-10 new-ticket-channels" onclick="{if $channel!=$menu_channel[0]}window.location.href = '{jurl 'support~ticket:create'}?channel={$menu_channel[0]}'{/if}">{i '32/'.$menu_channel[2]} <span class=" {if $channel==$menu_channel[0]}selected{/if}" href="?channel={$menu_channel[0]}">{$menu_channel[1]}</span></div>
		{/foreach}
		{/if}
	</div>
	
	{if $ig}</td><td valign="top" bgcolor="#181818">{/if}
	
	<div id="support-ticket-form" class="v big-frame account">
	{if $channel != 'intro'}
		<table class="form">
		<tr><td align="center">
			{if $ig}
				<table bgcolor="#335" style="font-weight: bold" cellspacing="0">
					{if $errors}
					<tr><td style="background-color:pink">{i '32/error'}</td><td width="100%" style="color: black; background-color:pink" height="35px">{t 'complete_all_fields'}</td></tr>
					{/if}
					<tr><td  height="32px" colspan="2">{t 'open_in_browser'}</td></tr>
					<tr><td>{i '32/world_go'}</td><td width="100%" height="32px"><a href="ah:open_url&{ryurl 'support~channel:index', array('name' => $ticket->channel)}">{t 'open_in_browser_link'}</a></td></tr>
				</table>
			{else}
			<form action="{jurl 'support~ticket:upload'}" id="new-ticket-screens" class="dropzone">{t 'add_screen'}</form>
			{/if}
		</td></tr>
		{form $ticket_form, 'support~ticket:add'}
		{if $channel == 'restore' || $channel == 'lost'}
			<tr><td align="center"><br />{$help}</td></tr>
			<tr><td><br /></td></tr>
		{/if}
			<input type="hidden" name="channel" value="{$channel}" />
			<fieldset>
				{formcontrols}
				{assign $display = true}
				
				{ifctrl 'another_char'}
					{assign $display = false}
				{/ifctrl}

				{ifctrl 'char'}
					{assign $display = false}
					{if $logged}
					<tr><td align="center">{t_ctrl_label}</td></tr>
					<tr><td align="center">{ctrl_control 'char', ['onchange' => 'newTicketOncharChange("'.$channel.'", this.value)']}</td></tr>
					{/if}
				{/ifctrl}
				
				{ifctrl 'for_char'}
					{assign $display = false}
					<tr> 
						<td align="center">
							<div name="for_char">
							{ctrl_control 'for_char', ['class' => "hidden"]}
							</div>
						</td></tr>

					{if $help}
					<tr><td align="center"><br />{$help}</td></tr>
					{/if}
					<tr><td><br /></td></tr>
				{/ifctrl}

				{ifctrl 'slotA'}
					{assign $display = false}
					<tr><td align="center">{t_ctrl_label}</td></tr>
					<tr><td align="center">{ctrl_control}</td></tr>
					<tr><td><br /></td></tr>
				{/ifctrl}
				
				{ifctrl 'name_props'}
					<tr><td align="center">{$help}</td></tr>
				{/ifctrl}
				
				{ifctrl 'platform'}
					<tr><td align="center">{$help}</td></tr>
				{/ifctrl}
				
				{ifctrl 'email'}
					{assign $display = false}
					{if !$logged}
						{assign $display = true}
					{/if}
				{/ifctrl}
				
				{ifctrl 'subject'}
					{assign $display = false}
					<tr><td align="center">{t_ctrl_label}</td></tr>
					<tr><td align="center">{if $ig}{ctrl_control 'subject', ['style' => 'width: 500px']}{else}{ctrl_control 'subject', ['style' => 'width: 80%']}{/if}
				{/ifctrl}

				{ifctrl 'section'}
					{assign $display = false}
					<tr><td align="center">{t_ctrl_label}</td></tr>
					<tr><td align="center">{ctrl_control 'section', ['name' => 'section', 'onchange' => 'updateNewTicketMessage(this.value)']}
					</td></tr>
				{/ifctrl}
				
				{ifctrl 'message'}
					{if $infos}
					<tr><td align="center"><label class="jforms-label">{$infos}</label></td></tr>
					{/if}
					{assign $display = false}
					{if $channel == 'rename'}
					<tr><td align="center">{ctrl_control 'message', ['rows' => 3]}</td></tr>
					{else}
					<tr><td align="center">{ctrl_control 'message', ['rows' => 7]}</td></tr>
					{/if}
				{/ifctrl}

				{ifctrl 'optional_mission_name'}
					{assign $display = false}
					<div name="optional_mission_name" {if !$ig}class="hidden"{/if}>
						<tr><td align="center">{t_ctrl_label}</td></tr>
						<tr><td align="center">{ctrl_control}</td></tr>
					</div>
				{/ifctrl}


				{ifctrl 'optional_mission_giver'}
					{assign $display = false}
					<div name="optional_mission_giver" {if !$ig}class="hidden"{/if}>
						<tr><td align="center">{t_ctrl_label}</td></tr>
						<tr><td align="center">{ctrl_control}</td></tr>
					</div>
				{/ifctrl}
				
				{ifctrl 'optional_crash'}
					{assign $display = false}
					<div name="optional_crash" {if !$ig}class="hidden"{/if}>
						<tr><td align="center">{t_ctrl_label}</td></tr>
						<tr><td align="center">{ctrl_control}</td></tr>
					</div>
				{/ifctrl}

				{if $channel != "" && $display}
					<tr><td align="center">{t_ctrl_label}</td></tr>
					<tr><td align="center">{ctrl_control}</td></tr>
				{/if}
				{/formcontrols}
			</fieldset>
			<tr><td id="notes" align="center">{$notes}</td></tr>
			{if $channel != ""}<tr><td align="center"><input type="submit" name="submit" value="{t 'me~submit'}" {if $ig}style="height: 32px; background-color: #353" {/if}id="support-ticket-submit" /></td></tr>{/if}
		</table>
		{/form}
		{else}
		<table width="100%">
			<tr style="background-color:pink; color: black; font-weight: bold"><td align="center">{t 'message_ticket_spam'}</td></tr>
			<tr><td height="20px"></td></tr>
			<tr><td width="100%" align="center" style="font-weight: bold">{t 'message_choose_cat'}</td></tr>
		</table>
		{/if}
	</div>
	{if $ig}</td></tr></table>{/if}
	</div>
</div>
{/if}

<script>
	var channel = "{$channel}"
	var url = "{jurl 'support~ticket:create'}?channel="
	var url_upload_delete = "{jurl 'support~ticket:uploadDelete'}?screen="
{literal}
	document.addEventListener('DOMContentLoaded', function() {
		$('.markItUpEditor').markItUp(markitup_markdown_settings);
		if (channel == 'technical')
			updateNewTicketMessage('');
	});
	Dropzone.options.newTicketScreens = {
		paramName: 'screen',
		maxFilesize: 10, // MB
		acceptedFiles: 'image/*',
		addRemoveLinks: true,
		thumbnailWidth: 90,
		init: function() {
			this.on('success', file => {
				var e = document.getElementById('jforms_support_ticket_'+channel+'_attachments');
				screens = [];
				if (e.value)
					screens = JSON.parse(e.value);
				screens.push(file.xhr.response);
				e.value = JSON.stringify(screens);
			});
			
			this.on('removedfile', file => {
				var e = document.getElementById('jforms_support_ticket_'+channel+'_attachments');
				screens = {};
				if (e.value)
					screens = JSON.parse(e.value);
				var index = screens.indexOf(file.xhr.response);
				screens.splice(index, 1);
				e.value = JSON.stringify(screens);
				runAjax(url_upload_delete+file.xhr.response);
			});
		}
	};

{/literal}
</script>

{if $ig}
<lua>
local char_select = {$char_select}
{literal}

	function TicketOnCharSelectChanged(show)
		-- Last item are another char
		if show == nil then
			if getUICaller() then
				getUI(__CURRENT_WINDOW__):showDiv("for_char",  getUICaller().selection == getUICaller().selectionNb - 1)
			end
		else
			getUI(__CURRENT_WINDOW__):showDiv("for_char", true)
		end
	end
	
	function TicketOnSectionSelectChanged()
		-- Last item are another char
		local sections = {nil, {"optional_crash"}, {"optional_mission_name", "optional_mission_giver"}, {"optional_crash"}}
		
		getUI(__CURRENT_WINDOW__):showDiv("optional_mission_name", false)
		getUI(__CURRENT_WINDOW__):showDiv("optional_mission_giver", false)
		getUI(__CURRENT_WINDOW__):showDiv("optional_crash", false)
		
		
		if getUICaller() then
			debug(getUICaller().selection)
		end
		
		local sel = getUICaller().selection
		if getUICaller() and sections[sel] then
			for _,i in pairs(sections[sel]) do
				getUI(__CURRENT_WINDOW__):showDiv(i, true)
			end
		end
	end
	
	getUI(__CURRENT_WINDOW__):showDiv("for_char", false)
	getUI(__CURRENT_WINDOW__):find("char").on_change = "TicketOnCharSelectChanged()"
	getUI(__CURRENT_WINDOW__):find("section").on_change = "TicketOnSectionSelectChanged()"
	
	TicketOnCharSelectChanged(char_select)
	TicketOnSectionSelectChanged()
</lua>
{/literal}
{/if}
