[color=#FFFF00]**{$section|upperfirst}**[/color]{if $for_char} / Char: [color=#B1FDFF]**{$for_char}**[/color]{/if}
{if $optional_mission_name}Mission: [color=#FFA500]**{$optional_mission_name}**[/color]
{/if}{if $optional_mission_giver}Giver: [color=#FFA500]**{$optional_mission_giver}**[/color]
{/if}{if $optional_crash}O.S: [color=#FFA500]**{$optional_crash}**[/color]
{/if}{$message}