{if $ig}
<style>
{literal}
	* {
		font-size: 14px;
	}
	
	.ticket {
		font-size: 13px;
	}
	
	.ticket-note-message {
		font-size: 13px;
	}
	
	#ticket pre {
		font-size: 13px;
	}
	
{/literal}
</style>
{/if}


<div id="ticket">
	
{if $ig}
	<table width="100%">
	<tr><td colspan="2" valign="middle" bgcolor="#555" height="32px">
	<table width="100%" cellspacing="0" cellpadding="5" ><tr><td>
		<div id="new-ticket-title">{t 'ticket'} #{$ticket->id}</div>
	</td>
	<td width="32px" bgcolor="#222"><a href="{jurl 'support~channel:index'}">{i '32/text_list_bullets'}</a></td>
	<td width="150px" bgcolor="#222"><a style="text-decoration: none; font-weight: bold" href="{jurl 'support~channel:index'}">{t 'back_to_ticket_list'}</a></td>
	</tr></table>
	</td></tr>
	</table>
{else}
	<div id="ticket-header">
		<div id="ticket-title" class="badge-secondary round bold bg-{$ticket_color}">
			<div class="orange">{if $dev}<a href="https://app.ryzom.com/app_db/?server=me.ryzom.com&db=ryzocket&edit=tickets&&username={$user_name|lower}&where%5Bid%5D={$ticket->id}" target="_blank">#{$ticket->id}</a>{else}#{$ticket->id}{/if}</div>
			<div class="expand"></div>
			<div id="ticket-subject">{$ticket->subject}</div>
			<div class="expand"></div>
			<div id="ticket-time">{$ticket->creation_date}</div>
			<div id="ticket-close"><a class="orange" href="{jurl 'support~channel:index', array('name' => $ticket->channel, 'show_user' => $ticket->user_id, 'email' => $email, 'token' => $token)}"><i class="fa-solid fa-rectangle-xmark"></i></a></div>
		</div>
		<div class="h badge-secondary bold bg-{$ticket_color}"style="gap: 5px; align-items: center" >
			{if $char_id}<a href="https://app.ryzom.com/app_admin/get_infos.php?type=cid&value={$char_id}" target="_blank">{$contact}</a>{else}
				<div>{$contact}</div>
				<div id="ticket-linked-accounts">
				{if $sgm}
					{foreach $linked_accounts as $la}
					<a class="over-black" href="https://app.ryzom.com/app_admin/get_infos.php?type=uid&value={$la[1]}" target="_blank">{$la[0]}</a>
					{/foreach}
				{/if}
				</div>
			{/if}
			<div>{if $sgm && $ticket->user_id} (Account: <a href="https://app.ryzom.com/app_admin/get_infos.php?type=uid&value={$ticket->user_id}" target="_blank">{$ticket->contact}</a>){/if}</div>
			<div style="font-size: 0.75em; min-width: 120px; text-align: right;">{t 'updated_since', [$ticket->remaining_time]}</div>
		</div>
		{if $agent == true}
		<div id="ticket-infos" class="bold badge-primary padding5">
			<div>
				<select id="select-channel" name="channel" onchange="show('ticket-validate-channel-change'); show('ticket-cancel-channel-change');">
					{foreach $channels as $channel}
						{if $channel->name != "My Tickets"}
						<option value="{$channel->name}" {if $channel->name == $ticket->channel}selected="selected"{/if}>{$channel->name}</option>
						{/if}
					{/foreach}
				</select>
			</div>
			<div>
				<select id="select-lang" name="lang" onchange="show('ticket-validate-lang-change'); show('ticket-cancel-lang-change');">
					<option value="de" {if $ticket->lang == 'de'}selected="selected"{/if}>🇩🇪 Deutsch</option>
					<option value="en" {if $ticket->lang == 'en'}selected="selected"{/if}>🇬🇧 English</option>
					<option value="es" {if $ticket->lang == 'es'}selected="selected"{/if}>🇪🇸 Español</option>
					<option value="fr" {if $ticket->lang == 'fr'}selected="selected"{/if}>🇫🇷 Français</option>
					<option value="ru" {if $ticket->lang == 'ru'}selected="selected"{/if}>🇷🇺 Pусский</option>
				</select>
			</div>
			<div class="expand"></div>
			<div>
				{if $ticket->assignee != ''}
				<img src="https://chat.ryzom.com/avatar/{$ticket->assignee|upperfirst}" style="height: 32px" />
				{elseif $agent == true}
					<input class="badge-primary round" type="button" onclick="window.location.replace('{$changeAgentUrl}&agent={$user_name}')" value="{t 'assign_myself'}">
				{/if}
				<select id="select-agent" name="agent" onchange="show('ticket-validate-agent-change'); show('ticket-cancel-agent-change');">
					{foreach $agents as $agent_id => $agent_name}
						<option value="{$agent_name}" {if $agent_name == ucfirst($ticket->assignee)}selected="selected"{/if}>{$agent_name}</option>
					{/foreach}
				</select>
			</div>
		</div>
		<div class="ticket-bar" class="padding5">
			<ul>
				<li id="ticket-validate-channel-change" class="hidden" onclick="hide('ticket-validate-channel-change'); hide('ticket-cancel-channel-change'); window.location.replace('{$changeChannelUrl}&channel='+encodeURI(document.getElementById('select-channel').options[document.getElementById('select-channel').selectedIndex].value)); return false;">{i '32/tick'} {t 'validate-channel-change'}</li>
				<li id="ticket-cancel-channel-change" class="hidden" onclick="hide('ticket-validate-channel-change'); hide('ticket-cancel-channel-change'); return false;">{i '32/cross'} {t 'cancel'}</li>
				<li id="ticket-validate-agent-change" class="hidden" onclick="hide('ticket-validate-agent-change'); hide('ticket-cancel-agent-change'); window.location.replace('{$changeAgentUrl}&agent='+encodeURI(document.getElementById('select-agent').options[document.getElementById('select-agent').selectedIndex].value)); return false;">{i '32/tick'} {t 'validate-agent-change'}</li>
				<li id="ticket-cancel-agent-change" class="hidden" onclick="hide('ticket-validate-agent-change'); hide('ticket-cancel-agent-change'); return false;">{i '32/cross'} {t 'cancel'}</li>
				<li id="ticket-validate-lang-change" class="hidden" onclick="hide('ticket-validate-lang-change'); hide('ticket-cancel-lang-change'); window.location.replace('{$changeLangUrl}&ticket_lang='+encodeURI(document.getElementById('select-lang').options[document.getElementById('select-lang').selectedIndex].value)); return false;">{i '32/tick'} {t 'validate-lang-change'}</li>
				<li id="ticket-cancel-lang-change" class="hidden" onclick="hide('ticket-validate-lang-change'); hide('ticket-cancel-lang-change'); return false;">{i '32/cross'} {t 'cancel'}</li>
			</ul>
		</div>
		{/if}
	</div>

{/if}
	<div id="ticket-messages" style="padding-top: 98px;">

	{if $ig}<table width="100%" cellspacing="0">{/if}
		
	{if $uploads}
		{if $ig}
			<tr bgcolor="#335"><td>{i '32/error'}</td><td height="32px">{t 'screens_available_outgame'}</td></tr>
			<tr bgcolor="#335"><td>{i '32/world_go'}</td><td width="100%" height="32px"><a href="ah:open_url&{ryurl 'support~channel:index', array('name' => $ticket->channel)}">{t 'open_in_browser_link'}</a></td></tr>
			<tr><td height="10px"></td></tr>
			</table><table width="100%" cellspacing="0" cellpadding="0">
		{else}
		<div id="uploads">
			{assign $i=1}
			{foreach $uploads as $file => $path}
				<a href="{$path}" data-lightbox="image-box" data-title="{$file}"><img style="width: 93px; max-height: 93px" src="{$path}" /></a>
			{/foreach}
		</div>
		<div class="clear" style="height: 5px"></div>
		{/if}
	{/if}
	
	{if $ig}
		<tr style="color: black" bgcolor="#aaa"><td height="24px" style="padding: 0 5px; font-weight: bold">{$ticket->subject}</td><td align="right">{$ticket->creation_date}</td></tr>
		<tr bgcolor="#222"><td colspan="2" style="padding: 10px 5px"><pre>{$ticket->message}</pre></td></tr>
		<tr bgcolor="#aaa"><td colspan="2" height="1px"></td></tr>
		<tr><td>&nbsp;</td></tr>
	{else}
		<div class="ticket-user">{$contact}</div>
		{if $ticket->lang != $lang && !$ticket->translation}<div class="ticket-deeplize" onclick="window.location.replace('{$deeplizeUrl}&dst_lang={$lang}'); return false;">Translate</div>{/if}
		<div class="ticket-date">{$ticket->creation_date}</div>
		<div class="ticket-message">
			<pre>{if $ticket->translation}{$ticket->translation}{else}{$ticket->message}{/if}</pre>
		</div>
	{/if}
	
	
	{foreach $notes as $note}
		{if $note->type == 'message'} 
			{if $ig}
				<tr style="{if $note->sender == 'agent'}background-color:#E1D0A8{else}background-color:#3A4147{/if}">
					<td height="24px" style="color: {if $note->sender == 'agent'}#1F4566{else}#9BC4A5{/if}; padding: 0 5px; font-weight: bold">{if $note->sender == 'agent'}{i '16/shield'}{$note->contact}{else}{i '16/status_online'}{t 'you'}{/if}</td>
					<td align="right" style="color: {if $note->sender == 'agent'}#333{else}#999{/if};" >{$note->creation_date}</td>
				</tr>
				<tr style="{if $note->sender == 'agent'}background-color:#E1D0A8; color:#111{else}background-color:#3A4147; color:#eee{/if}"><td colspan="2" style="padding: 10px 5px"><pre style="font-size: 14px; color:{if $note->sender == 'agent'}#111{else}#eee{/if}">{$note->message}</pre></td></tr>
				<tr><td>&nbsp;</td></tr>
			{else}
				<div class="ticket-{$note->sender}">{$note->contact}</div>
				{if $ticket->lang != $lang && !$note->translation}<div class="ticket-deeplize" onclick="window.location.replace('{$deeplizeUrl}&note_id={$note->id}&dst_lang={$lang}'); return false;">Translate</div>{/if}
				<div class="ticket-date">{$note->creation_date}</div>
				<div class="ticket-message-{$note->sender}">
					<pre>{if $note->translation}{$note->translation}{else}{$note->message}{/if}</pre>
				</div>
			{/if}
		{elseif $note->type == 'action' || $note->type == 'screen'}
			{if $ig}
				<tr><td height="24px" style="padding: 0 5px"><span style="color: orange">{$note->contact}</span> {$note->subject} {$note->message}</td><td align="right">{$note->creation_date}</td></tr>
				<tr><td>&nbsp;</td></tr>
			{else}
				<div class="ticket-action"><span>{$note->contact}</span> {$note->subject} <strong>{$note->message}</strong><div class="ticket-action-date">{$note->creation_date}</div></div>
			{/if}
		{elseif $note->type == 'note'}
			<div class="ticket-note">
				<div class="ticket-date black">{$note->creation_date}</div>
				<div>{$note->contact}</div>
				<div class="clear"></div>
				<div class="ticket-note-content">
					<pre>{$note->message}</pre>
					</div>
			</div>
		{/if}
		
	{/foreach}
	
	{if $ig}
		</table>
	
		<form action="{$formUrl}" id="form-send-message" method="POST">
			<table width="500px">
			<tr><td align="center"><textarea class="fullSize" rows="10" cols="50" name="message"></textarea></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td align="center"><input type="submit" name="submit" value="{t 'me~submit'}" style="height: 32px; background-color: #353" id="support-ticket-submit" /></td></tr>
			{if $ticket->type != "closed"}
			<tr><td height="60px" valign="bottom" align="left"><a href="{jurl 'support~ticket:close', array('ticket_id' => $ticket->id)}'">{i '32/cross'} {t 'close'}</a></td></tr>
			{/if}
		</table>
		</form>
		<br />
		<br />
	
	{else}
		{if $agent == true}
		<div class="ticket-bar" class="padding5">
			<ul>
				<li id="ticket-add-note-link" onclick="addClass('ticket-add-note-link', 'selected'); removeClass('ticket-send-message-link', 'selected'); show('ticket-add-note'); hide('ticket-send-message'); window.scrollTo(0,document.body.scrollHeight); return false;">{i '32/note_add'}  {t 'submit_note'}</li>
				<li id="ticket-send-message-link" onclick="removeClass('ticket-add-note-link', 'selected'); addClass('ticket-send-message-link', 'selected'); show('ticket-send-message'); hide('ticket-add-note'); window.scrollTo(0,document.body.scrollHeight); return false;">{i '32/comments'} {t 'ticket_answer'}</li>
				<li id="ticket-send-screen"><form action="{jurl 'support~ticket:upload', array('ticket' => $ticket->id)}" id="ticket-screens" class="dropzone">{t 'click_or_drop'}</form><div class="text-center"><a href="">{t 'refresh_page'}</a></div></li>
				{if $ticket->type != "closed" && $ticket->type != "spam"}
				<li id="ticket-closeit" class="bg-red radius5" onclick="openUrl('{jurl 'support~ticket:close', array('show_user' => $show_user, 'ticket_id' => $ticket->id)}')">{i '32/cross'} {t 'close'}</li>
				{/if}
				{if $ticket->type == "closed" || $ticket->type == "spam"}
				<li id="ticket-reopenit" class="bg-blue radius5" onclick="openUrl('{jurl 'support~ticket:open', array('show_user' => $show_user, 'ticket_id' => $ticket->id)}')">{i '32/tick'} Re-Open Ticket</li>
				{/if}
			</ul> 
		</div>
		{else}
		<div class="ticket-bar" class="padding5">
			<ul>
				<li id="ticket-send-message-link" onclick="removeClass('ticket-add-note-link', 'selected'); addClass('ticket-send-message-link', 'selected'); show('ticket-send-message'); hide('ticket-add-note'); window.scrollTo(0,document.body.scrollHeight); return false;">{i '32/comments'} {t 'ticket_answer'}</li>
				<li id="ticket-send-screen"><form action="{jurl 'support~ticket:upload', array('ticket' => $ticket->id)}" id="ticket-screens" class="dropzone">Click or drop image</form><div class="text-center"><a href="">{t 'refresh_page'}</a></div></li>
				{if $ticket->type != "closed"}
				<li id="ticket-closeit" class="bg-red radius5" onclick="openUrl('{jurl 'support~ticket:close', array('show_user' => $show_user, 'ticket_id' => $ticket->id, 'email' => $email, 'token'=> $token)}')">{i '32/cross'} {t 'close'}</li>
				{/if}
			</ul> 
		</div>
		{if $ticket->type == "closed"}
			{t 'answer_to_reopen'}
		{/if}
		{/if}

		<div id="ticket-send-message" class="hidden">
			<form action="{$formUrl}" id="form-send-message" method="POST">
				{if $agent == true}
					<textarea class="markItUp" id="ticket-agent-message" name="message"></textarea>
					<a class="button float-right" onclick="document.getElementById('form-send-message').submit();" value="submit">{t 'ticket_answer'}</a>
					<br />
				{else}
					<textarea class="fullSize" rows="10" id="ticket-player-message" name="message"></textarea>
					<a class="button float-right" onclick="document.getElementById('form-send-message').submit();" value="submit">{t 'ticket_answer'}</a>
					<br />
				{/if}
				<br />
			</form>
		</div>
		{if $agent == true}
		<div id="ticket-add-note" class="hidden">
			<form action="{$formUrl}" id="form-add-note" method="POST">
				<textarea class="markItUp" id="ticket-agent-note" name="note"></textarea>
				<a class="button float-right" onclick="document.getElementById('form-add-note').submit();" value="submit">{t 'submit_note'}</a>
				<br /><br />
			</form>
		</div>
		
		{/if}
	{/if}
	</div>
</div>

<script>
	var url_upload_delete = "{jurl 'support~ticket:uploadDelete'}?ticket={$ticket->id}&screen="
{literal}
	document.addEventListener('DOMContentLoaded', function() {
		$('.markItUp').markItUp(markitup_markdown_settings);
		window.scrollTo(0,document.body.scrollHeight);
		lightbox.option({
			'resizeDuration': 200,
			'wrapAround': true
		});
	});
	
	Dropzone.options.ticketScreens = {
		paramName: 'screen',
		maxFilesize: 10, // MB
		acceptedFiles: 'image/*',
		addRemoveLinks: true,
		thumbnailWidth: 90,
		init: function() {
			this.on('removedfile', file => {
				runAjax(url_upload_delete+file.xhr.response);
			});
		}
	};
{/literal}
</script>

{if $ig}
<lua>
	function ticketUpdateScrollBar()
		if getUI(__CURRENT_WINDOW__):find("scroll_bar").trackPos > 0 then
			getUI(__CURRENT_WINDOW__):find("scroll_bar").trackPos = 0
			setOnDraw(getUI(__CURRENT_WINDOW__), "")
		end
	end
	
	setOnDraw(getUI(__CURRENT_WINDOW__), "ticketUpdateScrollBar()")
</lua>
{/if}