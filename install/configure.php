<?php
/**
* @package   me
* @subpackage support
* @author    me.ryzom.com
* @copyright 2020 Winchgate Property
* @link      me.ryzom.com
* @license    All rights reserved
*/


class supportModuleConfigurator extends \Jelix\Installer\Module\Configurator {

    public function getDefaultParameters() {
        return array();
    }

    function configure(\Jelix\Installer\Module\API\ConfigurationHelpers $helpers) {

    }
}